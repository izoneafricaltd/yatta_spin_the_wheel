package com.ch.chewing.fresh.com.fresh_chewing_gum;

import android.app.AlertDialog;
import android.app.ListActivity;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.TextView;
import android.widget.Toast;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.StatusLine;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;


public class All_confirmed_visitors extends ListActivity {
	// Connection detector
	ConnectionDetector cd;

	// Alert dialog manager
	AlertDialogManager alert = new AlertDialogManager();

	// Progress Dialog
	private ProgressDialog pDialog;

	// Creating JSON Parser object
	JSONParser jsonParser = new JSONParser();

	ArrayList<HashMap<String, String>> taskList;

	// tasks JSONArray
	JSONArray tasks = null;

	// category id
	String task_id, task_name;
	String Vax;
	EditText searc_varX;
	// tasks JSON url
	// id - should be posted as GET params to get task list (ex: id = 5)
	private static final String URL_TASKS = "http://izonestudio.net/yatta_spin/fetch_data2.php";

	// ALL JSON node names
	private static final String TAG_TASK_JSON = "saerchz";
	private static final String TAG_VIS_ID = "id";
	private static final String TAG_VIS_FULLNAME = "name_xc";
	private static final String TAG_VIS_TEL = "tel_xc";
	private static final String TAG_VIS_EMAIL = "email_xc";
	private static final String TAG_VIS_STATUS = "status";
	private static final String TAG_VIS_DATE= "date_x";
	private static final String TAG_VIS_SEARCH= "sVarSearch";
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.all__brands);


		ImageView cancel=(ImageView) findViewById(R.id.cancel_4);
		cancel.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				finish();
				Intent i=new Intent(getApplicationContext(), Register_user.class);
				startActivity(i);
			}
		});

		cd = new ConnectionDetector(getApplicationContext());


		if (!cd.isConnectingToInternet()) {
			Toast.makeText(getApplicationContext(), "No internet !! Please access internet connection.", Toast.LENGTH_LONG).show();

		}else{


			taskList = new ArrayList<HashMap<String, String>>();

			// Loading task in Background Thread
			new LoadTasks().execute();
		}


		// get listview
		ListView lv = getListView();

		/**
		 * Listview on item click listener
		 * SingletaskActivity will be lauched by passing task id, cat id
		 * */


		//////////////////////////////////////////////////////////////////////////////////////////////////////
		lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
				//String  idx= ((TextView) view.findViewById(R.id.xsh_shop_id)).getText().toString();
			//	String  name= ((TextView) view.findViewById(R.id.xsh_shop_id)).getText().toString();

				//Intent i=new Intent(getApplicationContext(), Search_visitors.class);

				//i.putExtra("shop_id", idx);

				///startActivity(i);

				//String serial_no = ((TextView) view.findViewById(R.id.sf_sc_pin)).getText().toString();
				//String idx = ((TextView) view.findViewById(R.id.task_name)).getText().toString();
				//String descX = ((TextView) view.findViewById(R.id.task_desc)).getText().toString();



				Intent i = new Intent(getApplicationContext(), Register_user.class);
				//i.putExtra("n_id", idx);
			  //  i.putExtra("n_name", name);
				//i.putExtra("task_desc", descX);
				startActivity(i);
			}
		});
		//finish();
///////////////////////////////////////////////////////////////////////////////////////////////////////
	}
	@Override
	protected void onStop() {
		super.onStop();
		finish();
	}

	@Override
	protected void onDestroy() {
		super.onDestroy();

		finish();
	}
	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		if(keyCode== KeyEvent.KEYCODE_BACK) {
			// Toast.makeText(getApplicationContext(), "back press", Toast.LENGTH_LONG).show();
		}else{

		}
		return false;
		// Disable back button..............
	}
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.menu_main, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();

		//noinspection SimplifiableIfStatement
		if (id == R.id.action_settings) {

			//logout();
			return true;
		}
		if (id == R.id.action_settings) {

			//logout();
			return true;
		}

		return super.onOptionsItemSelected(item);
	}

	public String getHttpPost(String url,List<NameValuePair> params) {
		StringBuilder str = new StringBuilder();
		HttpClient client = new DefaultHttpClient();
		HttpPost httpPost = new HttpPost(url);

		try {
			httpPost.setEntity(new UrlEncodedFormEntity(params));
			HttpResponse response = client.execute(httpPost);
			StatusLine statusLine = response.getStatusLine();
			int statusCode = statusLine.getStatusCode();
			if (statusCode == 200) { // Status OK
				HttpEntity entity = response.getEntity();
				InputStream content = entity.getContent();
				BufferedReader reader = new BufferedReader(new InputStreamReader(content));
				String line;
				while ((line = reader.readLine()) != null) {
					str.append(line);
				}
			} else {
				Log.e("Log", "Failed to download result..");
			}
		} catch (ClientProtocolException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return str.toString();
	}

	/**
	 * Background Async Task to Load all tasks under one category
	 * */
	class LoadTasks extends AsyncTask<String, String, String> {

		/**
		 * Before starting background thread Show Progress Dialog
		 * */
		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			pDialog = new ProgressDialog(All_confirmed_visitors.this);
			pDialog.setMessage("Loading...");
			pDialog.setIndeterminate(false);
			pDialog.setCancelable(false);
			pDialog.show();
		}

		/**
		 * getting tasks json and parsing
		 * */
		protected String doInBackground(String... args) {
			// Building Parameters
			List<NameValuePair> params = new ArrayList<NameValuePair>();
//String cf="france";
			// post category id as GET parameter
			params.add(new BasicNameValuePair(TAG_VIS_SEARCH, Vax));
			//params.add(new BasicNameValuePair(TAG_DATE, task_datec));

			// getting JSON string from URL
			String json = jsonParser.makeHttpRequest(URL_TASKS, "GET",
					params);

			// Check your log cat for JSON reponse
			Log.d("task List JSON: ", json);

			try {
				JSONObject jObj = new JSONObject(json);
				if (jObj != null) {

					tasks = jObj.getJSONArray(TAG_TASK_JSON);
				///	String categ_id = jObj.getString(TAG_ID);
					//task_name = jObj.getString(TAG_CATEGORY);
					/*String categ_id = jObj.getString(TAG_ID);
					task_name = jObj.getString(TAG_CATEGORY);
					String categ_id = jObj.getString(TAG_ID);
					task_name = jObj.getString(TAG_CATEGORY);*/


					if (tasks != null) {
						// looping through All tasks
						for (int i = 0; i < tasks.length(); i++) {
							JSONObject c = tasks.getJSONObject(i);

							// Storing each json item in variable
							String s_vis_id = c.getString(TAG_VIS_ID);
						 ///String s_vis_imei= c.getString(TAG_VIS_IMEI);
							String s_vis_fullname = c.getString(TAG_VIS_FULLNAME);
							String s_vis_tel= c.getString(TAG_VIS_TEL);
							String s_vis_email = c.getString(TAG_VIS_EMAIL);
							String s_vis_status= c.getString(TAG_VIS_STATUS);
							//String s_vis_cat = c.getString(TAG_VIS_CAT);
						//	String s_vis_inv= c.getString(TAG_VIS_INV);
							String s_vis_date = c.getString(TAG_VIS_DATE);

							// creating new HashMap
							HashMap<String, String> map = new HashMap<String, String>();

							// adding each child node to HashMap key => value
							//map.put(TAG_ID, id);
							map.put(TAG_VIS_ID, s_vis_id);
						//	map.put(TAG_VIS_IMEI, s_vis_imei);
							map.put(TAG_VIS_FULLNAME, s_vis_fullname);
						    map.put(TAG_VIS_TEL, s_vis_tel);
							map.put(TAG_VIS_EMAIL, s_vis_email);
							map.put(TAG_VIS_STATUS, s_vis_status);
						//	map.put(TAG_VIS_CAT, s_vis_cat);
						//	map.put(TAG_VIS_INV, s_vis_inv);
							map.put(TAG_VIS_DATE, s_vis_date);
							//map.put(TAG_SHOP_BILL, s_shop_bill);
							//map.put(TAG_SHOP_DATE, s_shop_date);


							// adding HashList to ArrayList
							taskList.add(map);

						}

					} else {
						Log.d("tasks: ", "null");
						Toast.makeText(getApplicationContext(), "Results empty", Toast.LENGTH_LONG).show();
						finish();
					}
				}

			} catch (JSONException e) {
				e.printStackTrace();
			}

			return null;
		}

		/**
		 * After completing background task Dismiss the progress dialog
		 * **/
		protected void onPostExecute(String file_url) {

			if(taskList.size() > 0) {
			// dismiss the dialog after getting all tasks
			pDialog.dismiss();
			// updating UI from Background Thread
			runOnUiThread(new Runnable() {
				public void run() {

					ListAdapter adapter = new SimpleAdapter(
							All_confirmed_visitors.this, taskList,
							R.layout.visitors_ui,
							new String[]{
									//TAG_VIS_ID,
									TAG_VIS_FULLNAME,
									TAG_VIS_TEL,
									TAG_VIS_EMAIL,

									TAG_VIS_STATUS,
									//TAG_VIS_CAT,
									//TAG_VIS_INV,
									TAG_VIS_DATE
							},
							new int[]{
									//R.id.id_no,
									R.id.txt_namex,
									R.id.txt_tel,
									R.id.txt_email,
									R.id.txt_status,
									R.id.txt_date
							});// updating listview
					setListAdapter(adapter);

					// Change Activity Title with category name
					setTitle(task_name);
				}
			});

			}else{
				AlertDialog.Builder builder = new AlertDialog.Builder(All_confirmed_visitors.this);
				builder.setMessage("Data not available!")
						.setCancelable(false)
						.setPositiveButton("OK", new DialogInterface.OnClickListener() {
							public void onClick(DialogInterface dialog, int id) {
								//do things
								finish();
							}
						});
				AlertDialog alert = builder.create();
				alert.show();



			}


		}



	}
}