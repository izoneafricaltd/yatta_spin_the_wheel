package com.ch.chewing.fresh.com.fresh_chewing_gum;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Canvas;
import android.media.MediaPlayer;
import android.media.session.MediaController;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Toast;
import android.widget.VideoView;

import java.util.Timer;
import java.util.TimerTask;

/**
 * Created by peter on 11/14/2016.
 */
public class idle_window extends Activity {
    VideoView vd1,vd2;
    ImageView cont;
    Timer timer;
    TimerTask timerTask;
    private final static int NUM_PARTICLES = 25;
    private final static int FRAME_RATE = 30;
    private final static int LIFETIME = 300;
    private Handler mHandler;
    private View mFX;

    private View mFX_main;
    private Explosion mExplosion;
    ImageView iv;
    ImageView spinitx;
    //ImageView spin_angle;
    final Handler handler = new Handler();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.idle_windows);

        RelativeLayout layout = (RelativeLayout) findViewById(R.id.frame);
        spinitx = (ImageView) findViewById(R.id.frsh_pack);

        /*mFX = new View(this) {
            @Override
            protected void onDraw(Canvas c) {
                if (mExplosion!=null && !mExplosion.isDead())  {
                    mExplosion.update(c);
                    mHandler.removeCallbacks(mRunner);
                    mHandler.postDelayed(mRunner, FRAME_RATE);
                } else if (mExplosion!=null && mExplosion.isDead()) {
                    spinitx.setAlpha(1f);
                }
                super.onDraw(c);
            }
        };
        mFX.setLayoutParams(new RelativeLayout.LayoutParams(
                RelativeLayout.LayoutParams.MATCH_PARENT,
                RelativeLayout.LayoutParams.MATCH_PARENT));
        layout.addView(mFX);
        mHandler = new Handler();*/

        /*cont=(ImageView) findViewById(R.id.frsh_pack);
        cont.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


            }
        });*/
       // start_player();
        spinitx.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               /* if (mExplosion == null || mExplosion.isDead()) {
                    int[] loc = new int[2];
                    v.getLocationOnScreen(loc);
                    int offsetX = (int) (loc[0] + (v.getWidth() * .25));
                    int offsetY = (int) (loc[1] - (v.getHeight() * .5));
                    mExplosion = new Explosion(NUM_PARTICLES, offsetX, offsetY, getApplicationContext());
                    mHandler.removeCallbacks(mRunner);
                    mHandler.post(mRunner);

                    mHandler.removeCallbacks(mRunner);
                    mHandler.post(mRunner);
                    v.animate().alpha(0).setDuration(LIFETIME).start();
                    explode_sound();
                }*/
                explode_sound();
                start_reg();
            }
        });

        //startTimer();


    }

    private void explode_sound() {
        final MediaPlayer ourSong = MediaPlayer.create(getApplicationContext(), R.raw.xplode_kk);
        ourSong.start();
        //startTimer2();
    }

    /*private void startTimer2() {
        timer = new Timer();

        //initialize the TimerTask's job
        initializeTimerTask2();

        //schedule the timer, after the first 5000ms the TimerTask will run every 10000ms
        timer.schedule(timerTask, 1000, 600000); //
    }

    private void initializeTimerTask2() {
        timerTask = new TimerTask() {
            public void run() {

                //use a handler to run a toast that shows the current timestamp
                handler.post(new Runnable() {
                    public void run() {
                        ///////
///start_reg();


                    }
                });
            }
        };
    }*/

    private void start_reg() {
        Intent i = new Intent(getApplicationContext(), Register_user.class);
        startActivity(i);
        finish();
    }


    @Override
    protected void onStop() {
        super.onStop();
        finish();
        //timer.cancel();
        ///timer.di
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        finish();
       /// timer.cancel();
    }



    /*public void startTimer() {
        //set a new Timer
        timer = new Timer();

        //initialize the TimerTask's job
        initializeTimerTask();

        //schedule the timer, after the first 5000ms the TimerTask will run every 10000ms
        timer.schedule(timerTask, 1000, 53000); //
        //startService(new Intent(UnusualService.this, UnusualService.class);
    }
    public void initializeTimerTask() {

        timerTask = new TimerTask() {
            public void run() {

                //use a handler to run a toast that shows the current timestamp
                handler.post(new Runnable() {
                    public void run() {
                        ///////

                       // start_player();

                    }
                });
            }
        };
    }*/

 /*   private void start_player() {
        vd1 = (VideoView)this.findViewById(R.id.v1);
        String uri = "android.resource://" + getPackageName() + "/" + R.raw.yatta_grape;
        vd1.setVideoURI(Uri.parse(uri));
        vd1.start();
    }
    private Runnable mRunner = new Runnable() {
        @Override
        public void run() {
            mHandler.removeCallbacks(mRunner);
            mFX.invalidate();
        }*/
    //};

}
