package com.ch.chewing.fresh.com.fresh_chewing_gum;

import android.app.Activity;
import android.content.Intent;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.util.Log;
import android.widget.ProgressBar;
import android.widget.Toast;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.StatusLine;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.List;


public class splash_spin extends Activity {
    MediaPlayer ourSong;
    Boolean isInternetPresent = false;
    ConnectionDetector cd;

    // Alert dialog manager
    AlertDialogManager alert = new AlertDialogManager();

    // Connection detector class
    //ConnectionDetector cd;
    ProgressBar pgr;
    int progress=0;
    Handler h=new Handler();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.splashiz_spin);
     // startservice();
        check_sd_card();

        final MediaPlayer ourSong = MediaPlayer.create(getApplicationContext(), R.raw.app_task);
        ourSong.start();

        Thread timer = new Thread(){
            public void run(){

                try{
                    sleep(5000);


                }catch (InterruptedException e){
                    e.printStackTrace();


                }finally{
                    ourSong.release();
                    Intent i = new Intent(getApplicationContext(), Register_user.class);
                  startActivity(i);
                  finish();

                }

            }

        };


        timer.start();
        
        
    }

    private void check_sd_card() {

        if(Environment.getExternalStorageState().equals(Environment.MEDIA_MOUNTED))

        {
            chech_system_files();
            //state.setText("SD card is present");
        } else {
            Toast.makeText(getApplicationContext(), "SDCARD missing", Toast.LENGTH_SHORT).show();
            finish();
        }
    }

    private void chech_system_files() {

        File dir = new File(Environment.getExternalStorageDirectory() + "/yatta_spin");
        if(dir.exists() && dir.isDirectory()) {
            // do something here
            // Toast.makeText(getApplicationContext(), "Exists", Toast.LENGTH_SHORT).show();
            // play_music();
        }else{
            // Toast.makeText(getApplicationContext(), "Dont Exist",Toast.LENGTH_SHORT).show();
            create_folder();


        }
    }

    private void create_folder() {

        try {
            String newFolder = "/yatta_spin";
            String extSdcard = Environment.getExternalStorageDirectory().toString();
            File file = new File(extSdcard + newFolder);
            file.mkdir();

        } catch (Exception e) {
            e.printStackTrace();
        }
        // Toast.makeText(getApplicationContext(), "Folder done",Toast.LENGTH_SHORT).show();
        add_spin_reg();
        add_reg_names();
    }

    private void add_reg_names() {

        FileWriter fWriter;
        try{
            fWriter = new FileWriter("/sdcard/yatta_spin/yatta_spin_reg.csv");
            fWriter.write("");
            fWriter.flush();
            fWriter.close();
        }catch(Exception e){
            e.printStackTrace();
        }
        Toast.makeText(getApplicationContext(), "Initializing..",Toast.LENGTH_SHORT).show();
    }

    private void add_spin_reg() {

        FileWriter fWriter;
        try{
            fWriter = new FileWriter("/sdcard/yatta_spin/yatta_spin_2018.csv");
            fWriter.write("");
            fWriter.flush();
            fWriter.close();
        }catch(Exception e){
            e.printStackTrace();
        }
        Toast.makeText(getApplicationContext(), "Initializing..",Toast.LENGTH_SHORT).show();
    }

    @Override
    protected void onStop() {
        super.onStop();
        finish();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        finish();
    }

    private void startservice() {
        // TODO Auto-generated method stub
    startService(new Intent(getBaseContext(), sync_service.class));

    }


    public String getHttpPost(String url,List<NameValuePair> params) {
        StringBuilder str = new StringBuilder();
        HttpClient client = new DefaultHttpClient();
        HttpPost httpPost = new HttpPost(url);

        try {
            httpPost.setEntity(new UrlEncodedFormEntity(params));
            HttpResponse response = client.execute(httpPost);
            StatusLine statusLine = response.getStatusLine();
            int statusCode = statusLine.getStatusCode();
            if (statusCode == 200) { // Status OK
                HttpEntity entity = response.getEntity();
                InputStream content = entity.getContent();
                BufferedReader reader = new BufferedReader(new InputStreamReader(content));
                String line;
                while ((line = reader.readLine()) != null) {
                    str.append(line);
                }
            } else {
                Log.e("Log", "Failed to download result..");
            }
        } catch (ClientProtocolException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return str.toString();
    }



}


