package com.ch.chewing.fresh.com.fresh_chewing_gum;

import android.app.Activity;
import android.graphics.Canvas;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.RelativeLayout;

public class ExplodeActivity extends Activity {

	private final static int NUM_PARTICLES = 25;
	private final static int FRAME_RATE = 30;
	private final static int LIFETIME = 300;
	private Handler mHandler;
	private Handler mHandler2;

	private View mFX;
	private View mFX2;
	private View mFX3;
	private View mFX4;
	private View mFX5;
	private View mFX_main;
	private Explosion mExplosion;
	ImageView iv;
	ImageView iv2;
	ImageView iv3;
	ImageView iv4;
	ImageView iv5;
	//ImageView iv_ma;
	//String p=R.id.bomb_img;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		////////////////////////////////////////////
		RelativeLayout layout = (RelativeLayout) findViewById(R.id.frame);
		iv = (ImageView) findViewById(R.id.bomb_img);

		mFX = new View(this) {
			@Override
		    protected void onDraw(Canvas c) {
				if (mExplosion!=null && !mExplosion.isDead())  {
					mExplosion.update(c);
					mHandler.removeCallbacks(mRunner);
					mHandler.postDelayed(mRunner, FRAME_RATE);
				} else if (mExplosion!=null && mExplosion.isDead()) {
					iv.setAlpha(1f);
				}
				super.onDraw(c);
			}
		};
		mFX.setLayoutParams(new RelativeLayout.LayoutParams(
				RelativeLayout.LayoutParams.MATCH_PARENT,
				RelativeLayout.LayoutParams.MATCH_PARENT));
		layout.addView(mFX);
		mHandler = new Handler();

		//////////////////////////////////////////
		RelativeLayout layout_main = (RelativeLayout) findViewById(R.id.frame_main);
		iv = (ImageView) findViewById(R.id.bomb_img);

		mFX_main = new View(this) {
			@Override
			protected void onDraw(Canvas c) {
				if (mExplosion!=null && !mExplosion.isDead())  {
					mExplosion.update(c);
					mHandler.removeCallbacks(mRunner_main);
					mHandler.postDelayed(mRunner_main, FRAME_RATE);
				} else if (mExplosion!=null && mExplosion.isDead()) {
					iv.setAlpha(1f);
				}
				super.onDraw(c);
			}
		};
		mFX_main.setLayoutParams(new RelativeLayout.LayoutParams(
				RelativeLayout.LayoutParams.MATCH_PARENT,
				RelativeLayout.LayoutParams.MATCH_PARENT));
		layout_main.addView(mFX_main);
		mHandler2 = new Handler();
////////////////////////////////////////////////////////////////////////////////////////////////////////
		RelativeLayout layout_main2 = (RelativeLayout) findViewById(R.id.frame_main);
		iv2 = (ImageView) findViewById(R.id.bomb_img);

		mFX_main = new View(this) {
			@Override
			protected void onDraw(Canvas c) {
				if (mExplosion!=null && !mExplosion.isDead())  {
					mExplosion.update(c);
					mHandler.removeCallbacks(mRunner_main);
					mHandler.postDelayed(mRunner_main, FRAME_RATE);
				} else if (mExplosion!=null && mExplosion.isDead()) {
					iv2.setAlpha(1f);
				}
				super.onDraw(c);
			}
		};
		mFX_main.setLayoutParams(new RelativeLayout.LayoutParams(
				RelativeLayout.LayoutParams.MATCH_PARENT,
				RelativeLayout.LayoutParams.MATCH_PARENT));
		layout_main2.addView(mFX_main);
		mHandler2 = new Handler();
		///////////////////////////////////////////////////////////////////////////////////////////////////
		RelativeLayout layout_main3 = (RelativeLayout) findViewById(R.id.frame_main);
		iv3 = (ImageView) findViewById(R.id.bomb_img);

		mFX_main = new View(this) {
			@Override
			protected void onDraw(Canvas c) {
				if (mExplosion!=null && !mExplosion.isDead())  {
					mExplosion.update(c);
					mHandler.removeCallbacks(mRunner_main);
					mHandler.postDelayed(mRunner_main, FRAME_RATE);
				} else if (mExplosion!=null && mExplosion.isDead()) {
					iv3.setAlpha(1f);
				}
				super.onDraw(c);
			}
		};
		mFX_main.setLayoutParams(new RelativeLayout.LayoutParams(
				RelativeLayout.LayoutParams.MATCH_PARENT,
				RelativeLayout.LayoutParams.MATCH_PARENT));
		layout_main3.addView(mFX_main);
		mHandler2 = new Handler();
		/////////////////////////////////////////////////////////////////////////
		RelativeLayout layout_main4 = (RelativeLayout) findViewById(R.id.frame_main);
		iv4 = (ImageView) findViewById(R.id.bomb_img);

		mFX_main = new View(this) {
			@Override
			protected void onDraw(Canvas c) {
				if (mExplosion!=null && !mExplosion.isDead())  {
					mExplosion.update(c);
					mHandler.removeCallbacks(mRunner_main);
					mHandler.postDelayed(mRunner_main, FRAME_RATE);
				} else if (mExplosion!=null && mExplosion.isDead()) {
					iv4.setAlpha(1f);
				}
				super.onDraw(c);
			}
		};
		mFX_main.setLayoutParams(new RelativeLayout.LayoutParams(
				RelativeLayout.LayoutParams.MATCH_PARENT,
				RelativeLayout.LayoutParams.MATCH_PARENT));
		layout_main4.addView(mFX_main);
		mHandler2 = new Handler();
		/////////////////////////////////////////////////////////////////////////////////////////
		RelativeLayout layout_main5 = (RelativeLayout) findViewById(R.id.frame_main);
		iv5 = (ImageView) findViewById(R.id.bomb_img);

		mFX_main = new View(this) {
			@Override
			protected void onDraw(Canvas c) {
				if (mExplosion!=null && !mExplosion.isDead())  {
					mExplosion.update(c);
					mHandler.removeCallbacks(mRunner_main);
					mHandler.postDelayed(mRunner_main, FRAME_RATE);
				} else if (mExplosion!=null && mExplosion.isDead()) {
					iv5.setAlpha(1f);
				}
				super.onDraw(c);
			}
		};
		mFX_main.setLayoutParams(new RelativeLayout.LayoutParams(
				RelativeLayout.LayoutParams.MATCH_PARENT,
				RelativeLayout.LayoutParams.MATCH_PARENT));
		layout_main5.addView(mFX_main);
		mHandler2 = new Handler();

		/////////////////////////////////////////////////////////////////////////////////////////////////

		RelativeLayout layout5 = (RelativeLayout) findViewById(R.id.frame5);
		iv5 = (ImageView) findViewById(R.id.bomb_img5);



		mFX5 = new View(this) {
			@Override
			protected void onDraw(Canvas c) {
				if (mExplosion!=null && !mExplosion.isDead())  {
					mExplosion.update(c);
					mHandler.removeCallbacks(mRunner5);
					mHandler.postDelayed(mRunner5, FRAME_RATE);
				} else if (mExplosion!=null && mExplosion.isDead()) {
					iv5.setAlpha(1f);
				}
				super.onDraw(c);
			}
		};
		mFX5.setLayoutParams(new RelativeLayout.LayoutParams(
				RelativeLayout.LayoutParams.MATCH_PARENT,
				RelativeLayout.LayoutParams.MATCH_PARENT));
		layout5.addView(mFX5);
		mHandler = new Handler();


		//////////////////////////////////////////////////////////////////////
		RelativeLayout layout2 = (RelativeLayout) findViewById(R.id.frame2);
		iv2 = (ImageView) findViewById(R.id.bomb_img2);
		mFX2 = new View(this) {
			@Override
			protected void onDraw(Canvas c) {
				if (mExplosion!=null && !mExplosion.isDead())  {
					mExplosion.update(c);
					mHandler.removeCallbacks(mRunner2);
					mHandler.postDelayed(mRunner2, FRAME_RATE);
				} else if (mExplosion!=null && mExplosion.isDead()) {
					iv2.setAlpha(1f);
				}
				super.onDraw(c);
			}
		};
		mFX2.setLayoutParams(new RelativeLayout.LayoutParams(
				RelativeLayout.LayoutParams.MATCH_PARENT,
				RelativeLayout.LayoutParams.MATCH_PARENT));
		layout2.addView(mFX2);
		mHandler = new Handler();
		//////////////////////////////////////////////
		RelativeLayout layout4 = (RelativeLayout) findViewById(R.id.frame4);
		iv4 = (ImageView) findViewById(R.id.bomb_img4);
		mFX4 = new View(this) {
			@Override
			protected void onDraw(Canvas c) {
				if (mExplosion!=null && !mExplosion.isDead())  {
					mExplosion.update(c);
					mHandler.removeCallbacks(mRunner4);
					mHandler.postDelayed(mRunner4, FRAME_RATE);
				} else if (mExplosion!=null && mExplosion.isDead()) {
					iv4.setAlpha(1f);
				}
				super.onDraw(c);
			}
		};
		mFX4.setLayoutParams(new RelativeLayout.LayoutParams(
				RelativeLayout.LayoutParams.MATCH_PARENT,
				RelativeLayout.LayoutParams.MATCH_PARENT));
		layout4.addView(mFX4);
		mHandler = new Handler();
		//////////////////////////////////////////
		RelativeLayout layout3 = (RelativeLayout) findViewById(R.id.frame3);
		iv3 = (ImageView) findViewById(R.id.bomb_img3);
		mFX3 = new View(this) {
			@Override
			protected void onDraw(Canvas c) {
				if (mExplosion!=null && !mExplosion.isDead())  {
					mExplosion.update(c);
					mHandler.removeCallbacks(mRunner3);
					mHandler.postDelayed(mRunner3, FRAME_RATE);
				} else if (mExplosion!=null && mExplosion.isDead()) {
					iv3.setAlpha(1f);
				}
				super.onDraw(c);
			}
		};
		mFX3.setLayoutParams(new RelativeLayout.LayoutParams(
				RelativeLayout.LayoutParams.MATCH_PARENT,
				RelativeLayout.LayoutParams.MATCH_PARENT));
		layout3.addView(mFX3);
		mHandler = new Handler();


		//////////////////////////////////////

iv.setOnClickListener(new OnClickListener() {
	@Override
	public void onClick(View v) {
		if (mExplosion==null || mExplosion.isDead()) {
			int[] loc = new int[2];
			v.getLocationOnScreen(loc);
			int offsetX = (int) (loc[0] + (v.getWidth()*.25));
			int offsetY = (int) (loc[1] - (v.getHeight()*.5));
			mExplosion = new Explosion(NUM_PARTICLES, offsetX, offsetY, getApplicationContext());
			mHandler.removeCallbacks(mRunner);
			mHandler.post(mRunner);

			mHandler2.removeCallbacks(mRunner_main);
			mHandler2.post(mRunner_main);
			v.animate().alpha(0).setDuration(LIFETIME).start();
			explode();
		}

	}
});
		iv5.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				if (mExplosion==null || mExplosion.isDead()) {
					int[] loc = new int[2];
					v.getLocationOnScreen(loc);
					int offsetX = (int) (loc[0] + (v.getWidth()*.25));
					int offsetY = (int) (loc[1] - (v.getHeight()*.5));
					mExplosion = new Explosion(NUM_PARTICLES, offsetX, offsetY, getApplicationContext());
					mHandler.removeCallbacks(mRunner5);
					mHandler.post(mRunner5);
					mHandler2.removeCallbacks(mRunner_main);
					mHandler2.post(mRunner_main);
					v.animate().alpha(0).setDuration(LIFETIME).start();
					///////////////

					explode();
					/////////////////////////////////
				}

			}
		});
		iv2.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v2) {
				if (mExplosion==null || mExplosion.isDead()) {
					int[] loc = new int[2];
					v2.getLocationOnScreen(loc);
					int offsetX = (int) (loc[0] + (v2.getWidth()*.25));
					int offsetY = (int) (loc[1] - (v2.getHeight()*.5));
					mExplosion = new Explosion(NUM_PARTICLES, offsetX, offsetY, getApplicationContext());
					mHandler.removeCallbacks(mRunner2);
					mHandler.post(mRunner2);
					mHandler2.removeCallbacks(mRunner_main);
					mHandler2.post(mRunner_main);
					v2.animate().alpha(0).setDuration(LIFETIME).start();
					explode();
				}

			}
		});
		iv3.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v3) {

				if (mExplosion==null || mExplosion.isDead()) {
					int[] loc = new int[2];
					v3.getLocationOnScreen(loc);
					int offsetX = (int) (loc[0] + (v3.getWidth()*.25));
					int offsetY = (int) (loc[1] - (v3.getHeight()*.5));
					mExplosion = new Explosion(NUM_PARTICLES, offsetX, offsetY, getApplicationContext());
					mHandler.removeCallbacks(mRunner3);
					mHandler.post(mRunner3);
					mHandler2.removeCallbacks(mRunner_main);
					mHandler2.post(mRunner_main);
					v3.animate().alpha(0).setDuration(LIFETIME).start();

					explode();
				}

			}
		});
		iv4.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v4) {
				if (mExplosion==null || mExplosion.isDead()) {
					int[] loc = new int[2];
					v4.getLocationOnScreen(loc);
					int offsetX = (int) (loc[0] + (v4.getWidth()*.25));
					int offsetY = (int) (loc[1] - (v4.getHeight()*.5));
					mExplosion = new Explosion(NUM_PARTICLES, offsetX, offsetY, getApplicationContext());
					mHandler.removeCallbacks(mRunner4);
					mHandler.post(mRunner4);
					mHandler2.removeCallbacks(mRunner_main);
					mHandler2.post(mRunner_main);
					v4.animate().alpha(0).setDuration(LIFETIME).start();
					explode();
				}

			}
		});
	}

	private void explode() {


		final MediaPlayer ourSong = MediaPlayer.create(getApplicationContext(), R.raw.xplode_kk);
		ourSong.start();
	}


	private Runnable mRunner = new Runnable() {
		@Override
		public void run() {
			mHandler.removeCallbacks(mRunner);
			mFX.invalidate();
		}
	};
	private Runnable mRunner2 = new Runnable() {
		@Override
		public void run() {
			mHandler.removeCallbacks(mRunner2);
			mFX2.invalidate();
		}
	};
	private Runnable mRunner3 = new Runnable() {
		@Override
		public void run() {
			mHandler.removeCallbacks(mRunner3);
			mFX3.invalidate();
		}
	};
	private Runnable mRunner4 = new Runnable() {
		@Override
		public void run() {
			mHandler.removeCallbacks(mRunner4);
			mFX4.invalidate();
		}
	};
	private Runnable mRunner5 = new Runnable() {
		@Override
		public void run() {
			mHandler.removeCallbacks(mRunner5);
			mFX5.invalidate();
		}
	};
	private Runnable mRunner_main = new Runnable() {
		@Override
		public void run() {
			mHandler2.removeCallbacks(mRunner_main);
			mFX_main.invalidate();
		}
	};

}
