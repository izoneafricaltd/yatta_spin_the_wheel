package com.ch.chewing.fresh.com.fresh_chewing_gum;
import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Intent;
import android.os.Handler;
import android.os.IBinder;
import android.os.StrictMode;
import android.support.v4.app.NotificationCompat;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.widget.Toast;

import com.opencsv.CSVReader;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.StatusLine;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

public class sync_service extends Service {

	Timer timer;
	TimerTask timerTask;


	final Handler handler = new Handler();

	ConnectionDetector cd;
	////////////////////////////
	String REG_DETAILS_IMEI;
	String REG_DETAILS_FULL_NAME;
	String REG_DETAILS_ID_NUMBER;
	String REG_DETAILS_TEL;
	String REG_DETAILS_DATETIMEX;
	String ACC_IMEIX;
	String ACC_USERNAMEXP;
	String ACC_PASSWORD;
	String ACC_DATETIME;
	String WON_IMEI_XP;
	String WON_ID_NO_XP;
	String WON_MER_NAME_XP;
	String WON_DATETIME_XP;
	String MER_QNTY_IMEI;
	String MER_QNTY_NAME;
	String MER_QNTY_DESC;
	String MER_QNTY_QNTY;
	String MER_QNTY_DATE;
	String var1,var2,var3,var4,var5,var6,var7,var8;
	/////////////////////
	String MER_QNTY_BOX;
	String MER_QNTY_CAP;
	String MER_QNTY_POLO;
	String MER_QNTY_R_NECK;
	String MER_QNTY_CIG;
	String MER_QNTY_RING;
	///////
	String cur_imei;
	String cur_qty_polo;
	String cur_qnt_round_nec;
	String cur_qty_match_box;
	String cur_qqnt_cigaretes;
	String cur__qnt_cap;
	String cur_qnt_key_ring;
	String cur_qnt_date;

	@Override
	public IBinder onBind(Intent intent) {
		//startTimer();
		return null;
	}

	@Override
	public void onStart(Intent intent, int startId) {
		// TODO Auto-generated method stub
		//startTimer();
		super.onStart(intent, startId);
	}




	@SuppressLint("NewApi")
	@Override
	public void onCreate() {
		super.onCreate();

		//	startTimer();

	}


	public void startTimer() {
		//set a new Timer
		timer = new Timer();

		//initialize the TimerTask's job
		initializeTimerTask();

		//schedule the timer, after the first 5000ms the TimerTask will run every 10000ms
		timer.schedule(timerTask, 5000, 360000000); //
		//startService(new Intent(UnusualService.this, UnusualService.class);
	}

	public void initializeTimerTask() {

		timerTask = new TimerTask() {
			public void run() {

				//use a handler to run a toast that shows the current timestamp
				handler.post(new Runnable() {
					public void run() {
						///////

						cd = new ConnectionDetector(getApplicationContext());


						if (!cd.isConnectingToInternet()) {
							Toast.makeText(getApplicationContext(), "No internet !! Please access internet  connection to sync your data.", Toast.LENGTH_LONG).show();

						}else{

							upload_names();
							upload_scores();


						}

					}
				});
			}
		};
	}

	private void upload_scores() {

		try {
			String[] row = null;
			//String csvFilename = "/sdcard/duka_mkononi/add_brand.csv";
			String csvFilename = "/sdcard/yatta_spin/yatta_spin_2018.csv";
			CSVReader csvReader = new CSVReader(new FileReader(csvFilename));
			List content = csvReader.readAll();

			for (Object object : content) {
				row = (String[]) object;

				String check=row[0];
				if(check.length()==0){

					//Toast.makeText(getApplicationContext(), "Empty line.....!.", Toast.LENGTH_SHORT).show();

					/// return false;
				}else{

					//Toast.makeText(getApplicationContext(), "Processing...please wait.....!.", Toast.LENGTH_SHORT).show();


					var1=row[0];
					var2=row[1];
					var3=row[2];
					var4=row[3];


					Toast.makeText(getApplicationContext(), "Syncing 2 of 2....", Toast.LENGTH_SHORT).show();

					upload_score_reg();
				}
			}
//...
			csvReader.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
		stopService(new Intent(getBaseContext(), sync_service.class));
	}

	private boolean upload_score_reg() {
		final AlertDialog.Builder ad = new AlertDialog.Builder(this);

		ad.setTitle("Error! ");
		ad.setIcon(android.R.drawable.btn_star_big_on);
		ad.setPositiveButton("Close", null);



		TelephonyManager tm = (TelephonyManager)getSystemService(TELEPHONY_SERVICE);
		String imei2= tm.getDeviceId();
///
		String url = "http://izonestudio.net/yatta_spin/insert_data1.php";

		List<NameValuePair> params = new ArrayList<NameValuePair>();


		params.add(new BasicNameValuePair("svar1", var1));
		params.add(new BasicNameValuePair("svar2", var2));
		params.add(new BasicNameValuePair("svar3", var3));
		params.add(new BasicNameValuePair("svar4", var4));
		//params.add(new BasicNameValuePair("scate", var5));
	//	params.add(new BasicNameValuePair("sinv", var6));
		//params.add(new BasicNameValuePair("sstatus", var7));
	//	params.add(new BasicNameValuePair("sdate", var8));

		String resultServer  = getHttpPost(url,params);

		/*** Default Value ***/
		String strStatusID = "0";
		String strError = "Internet problem!";

		JSONObject c;
		try {
			c = new JSONObject(resultServer);
			strStatusID = c.getString("StatusID");
			strError = c.getString("Error");
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		// Prepare Save Data
		if(strStatusID.equals("0"))
		{
			ad.setMessage(strError);
			ad.show();
		}
		else
		{
			//Toast.makeText(getApplicationContext(), "Added", Toast.LENGTH_LONG).show();

		}


		return true;
	}


	private void upload_names() {
		try {
			String[] row = null;
			//String csvFilename = "/sdcard/duka_mkononi/add_brand.csv";
			String csvFilename = "/sdcard/yatta_spin/yatta_spin_reg.csv";
			CSVReader csvReader = new CSVReader(new FileReader(csvFilename));
			List content = csvReader.readAll();

			for (Object object : content) {
				row = (String[]) object;

				String check=row[0];
				if(check.length()==0){

					//Toast.makeText(getApplicationContext(), "Empty line.....!.", Toast.LENGTH_SHORT).show();

					/// return false;
				}else{

					//Toast.makeText(getApplicationContext(), "Processing...please wait.....!.", Toast.LENGTH_SHORT).show();


					var1=row[0];
					var2=row[1];
					var3=row[2];
					var4=row[3];
					var5=row[4];
					//var6=row[5];
					//var7=row[6];
					//var8=row[7];

					//Toast.makeText(getApplication(),"Syncing..1 of 4...",Toast.LENGTH_SHORT).show();
					Toast.makeText(getApplicationContext(), "Syncing 1 of 2....", Toast.LENGTH_SHORT).show();
					//System.out.println(row[0]
					//    + " # " + row[1]
					//  + " #  " + row[2]);
					///upload_names_reg();
					upload_user_reg();
				}
			}
//...
			csvReader.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	//////////////////////////////////////



	public boolean upload_user_reg()
	{


		final AlertDialog.Builder ad = new AlertDialog.Builder(this);

		ad.setTitle("Error! ");
		ad.setIcon(android.R.drawable.btn_star_big_on);
		ad.setPositiveButton("Close", null);



		TelephonyManager tm = (TelephonyManager)getSystemService(TELEPHONY_SERVICE);
		String imei2= tm.getDeviceId();
///
		String url = "http://izonestudio.net/yatta_spin/insert_data2.php";

		List<NameValuePair> params = new ArrayList<NameValuePair>();


		params.add(new BasicNameValuePair("svar1", var1));
		params.add(new BasicNameValuePair("svar2", var2));
		params.add(new BasicNameValuePair("svar3", var3));
		params.add(new BasicNameValuePair("svar4", var4));
		params.add(new BasicNameValuePair("svar5", var5));
		//params.add(new BasicNameValuePair("sinv", var6));
		//params.add(new BasicNameValuePair("sstatus", var7));
		//params.add(new BasicNameValuePair("sdate", var8));

		String resultServer  = getHttpPost(url,params);

		/*** Default Value ***/
		String strStatusID = "0";
		String strError = "Internet problem!";

		JSONObject c;
		try {
			c = new JSONObject(resultServer);
			strStatusID = c.getString("StatusID");
			strError = c.getString("Error");
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		// Prepare Save Data
		if(strStatusID.equals("0"))
		{
			ad.setMessage(strError);
			ad.show();
		}
		else
		{
			//Toast.makeText(getApplicationContext(), "Added", Toast.LENGTH_LONG).show();

		}


		return true;
	}





	@SuppressLint("NewApi")
	@Override
	public int onStartCommand(final Intent intent, int flags, int startId) {


		if (android.os.Build.VERSION.SDK_INT > 9) {
			StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
			StrictMode.setThreadPolicy(policy);
		}
		startTimer();

		NotificationManager mNotificationManager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);

		Intent bIntent = new Intent(this, splash_spin.class);
		PendingIntent pbIntent = PendingIntent.getActivity(this, 0, bIntent,0);

		NotificationCompat.Builder bBuilder =
				new NotificationCompat.Builder(this)
						.setSmallIcon(R.drawable.success)
						.setContentTitle("Spin the wheeel service")
						.setContentText("Spin the wheeel sync")
						.setOngoing(true)
						.setContentIntent(pbIntent);
		Notification barNotif = bBuilder.build();
		this.startForeground(1, barNotif);


		// TODO Auto-generated method stub

		super.onStartCommand(intent, flags, startId);





		return START_STICKY;
	}




	public String getHttpPost(String url,List<NameValuePair> params) {
		StringBuilder str = new StringBuilder();
		HttpClient client = new DefaultHttpClient();
		HttpPost httpPost = new HttpPost(url);

		try {
			httpPost.setEntity(new UrlEncodedFormEntity(params));
			HttpResponse response = client.execute(httpPost);
			StatusLine statusLine = response.getStatusLine();
			int statusCode = statusLine.getStatusCode();
			if (statusCode == 200) { // Status OK
				HttpEntity entity = response.getEntity();
				InputStream content = entity.getContent();
				BufferedReader reader = new BufferedReader(new InputStreamReader(content));
				String line;
				while ((line = reader.readLine()) != null) {
					str.append(line);
				}
			} else {
				Log.e("Log", "Failed to download result..");
			}
		} catch (ClientProtocolException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return str.toString();
	}



}