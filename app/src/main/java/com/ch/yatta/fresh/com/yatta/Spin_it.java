package com.ch.chewing.fresh.com.fresh_chewing_gum;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Canvas;
import android.graphics.Typeface;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.os.Handler;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.animation.RotateAnimation;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.opencsv.CSVReader;

import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Random;
import java.util.Timer;
import java.util.TimerTask;

/**
 * Created by peter on 10/12/2016.
 */
public class Spin_it extends Activity
{

    Typeface tf1;
    Timer timer;
    TimerTask timerTask;
   String  spin_status;
    //we are going to use a handler to be able to run in our TimerTask
    final Handler handler = new Handler();
    Button spinitx;
    ImageView spin_angle;
    String random;
    float Rota;
    private int mCurrRotation = 0;
    String merchendize_xc;
    String imei_xc;
    String id_number;
    String id_name;
    TextView  nane_xvccb;
    String formattedDate;
    ///////////////////////////
    double cur_qty_polo;
    double cur_qnt_round_nec;
    double cur_qty_match_box;
    double cur_qqnt_cigaretes;
    double cur__qnt_cap;
    double cur_qnt_key_ring;
    ////////////////
    String in_qnty_match;
    String in_qnty_cig;
    String in_qnty_ring;
    String in_qnty_pol;
    String in_qnty_round;
    String in_qnty_cap;
    /////////////////////////
    String af_qnty_match;
    String af_qnty_cig;
    String af_qnty_ring;
    String af_qnty_pol;
    String af_qnty_round;
    String af_qnty_cap;
    /////////////////////////
    String af_2_qnty_match;
    String af_2_qnty_cig;
    String af_2_qnty_ring;
    String af_2_qnty_pol;
    String af_2_qnty_round;
    String af_2_qnty_cap;
    ////////////////////////////

    int down_coun_match;
    int down_coun_cig;
    int down_coun_ring;
    int down_coun_pol;
   int down_coun_round;
    int down_coun_cap;

    private final static int NUM_PARTICLES = 25;
    private final static int FRAME_RATE = 30;
    private final static int LIFETIME = 300;
    private Handler mHandler;
    private View mFX;

    private View mFX_main;
    private Explosion mExplosion;
    ImageView iv;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.spin_it);

        RelativeLayout layout = (RelativeLayout) findViewById(R.id.frame);
        spinitx = (Button) findViewById(R.id.frsh_pack);

        mFX = new View(this) {
            @Override
            protected void onDraw(Canvas c) {
                if (mExplosion!=null && !mExplosion.isDead())  {
                    mExplosion.update(c);
                    mHandler.removeCallbacks(mRunner);
                    mHandler.postDelayed(mRunner, FRAME_RATE);
                } else if (mExplosion!=null && mExplosion.isDead()) {
                    spinitx.setAlpha(1f);
                }
                super.onDraw(c);
            }
        };
        mFX.setLayoutParams(new RelativeLayout.LayoutParams(
                RelativeLayout.LayoutParams.MATCH_PARENT,
                RelativeLayout.LayoutParams.MATCH_PARENT));
        layout.addView(mFX);
        mHandler = new Handler();
        //////////////////////////////////////////////
      //  update_after_qnty();
///calculate_qnty();
       /// start_spin();
       // explode();
        Intent i = getIntent();
        imei_xc = i.getStringExtra("sx_imei");
        id_number = i.getStringExtra("sx_id");
        id_name = i.getStringExtra("sx_name");


       // spinitx=(ImageView) findViewById(R.id.frsh_pack);
        spin_angle=(ImageView) findViewById(R.id.wheeel_xc);
        nane_xvccb=(TextView) findViewById(R.id.name_xcp);

        tf1= Typeface.createFromAsset(getAssets(),"bernierregular.ttf");
        nane_xvccb.setTypeface(tf1);
        spinitx.setTypeface(tf1);

        /*spinitx.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                explode();
                start_spin();
            }
        });*/
        spin_angle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                start_spin();
            }
        });
        spinitx.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mExplosion == null || mExplosion.isDead()) {
                    int[] loc = new int[2];
                    v.getLocationOnScreen(loc);
                    int offsetX = (int) (loc[0] + (v.getWidth() * .25));
                    int offsetY = (int) (loc[1] - (v.getHeight() * .5));
                    mExplosion = new Explosion(NUM_PARTICLES, offsetX, offsetY, getApplicationContext());
                    mHandler.removeCallbacks(mRunner);
                    mHandler.post(mRunner);

                    mHandler.removeCallbacks(mRunner);
                    mHandler.post(mRunner);
                    v.animate().alpha(0).setDuration(LIFETIME).start();
                    explode_sound();
                }

            }
        });

        setdata();
    }

    @Override
    protected void onPause() {
        super.onPause();

        finish();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        finish();
    }



   /* private void update_after_qnty() {

        count_match_box();
        count_cap();
        count_polo();
        count_roun_neck();
        count_ciga();
        count_ring_key();
        writ_after_count();

    }*/

    /*private void writ_after_count() {
        formattedDate = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(Calendar.getInstance().getTime());
        erase();
        File file = new File("/sdcard/spin_it/after_quantity.csv");
        try {
            //create a filewriter and set append modus to true
            //File file = new File("/sdcard/spin_it/merc_quantity.csv");
            FileWriter fw = new FileWriter(file, true);
            // fw.append(System.getProperty("line.separator"));

            fw.append(down_coun_match+",");
            fw.append(down_coun_cap+",");
            fw.append(down_coun_pol+",");
            fw.append(down_coun_round+",");
            fw.append(down_coun_cig+",");
            fw.append(down_coun_ring+",");
            fw.append("date:"+ formattedDate+"");

            // fw.append("DATETIME:"+ phonepx+",");
            // fw.append(System.getProperty("line.separator"));
            // fw.append("QN1b:" + one_b + "--NEXT QNS--");

            fw.close();
            //end();

        } catch (IOException e) {
            Log.w("ExternalStorage", "Error writing " + file, e);
        }
    }*/

    /*private void erase() {
        try {
            FileWriter fw = new FileWriter("/sdcard/spin_it/after_quantity.csv/", false);
            PrintWriter pw = new PrintWriter(fw, false);
            pw.flush();
            pw.close();
            fw.close();
        } catch (IOException e) {
            Log.w("ExternalStorage", "Error writing ", e);
        }
    }

    private void count_ring_key() {
        try {
            String[] row = null;
            int count = 0;
            String csvFilename = "/sdcard/spin_it/ded_ring.csv";

            CSVReader csvReader = new CSVReader(new FileReader(csvFilename));
            List content = csvReader.readAll();

            for (Object object : content) {
                row = (String[]) object;
                count++;
                ////Toast.makeText(getApplication(),"Syncing..3 of 4...",Toast.LENGTH_SHORT).show();
                //Toast.makeText(getApplicationContext(), "3 of 3", Toast.LENGTH_SHORT).show();
                //upload_mer_qnty_reg();
            }

            down_coun_ring=count-1;
//...
            csvReader.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void count_ciga() {
        try {
            String[] row = null;
            int count = 0;
            String csvFilename = "/sdcard/spin_it/ded_cig.csv";

            CSVReader csvReader = new CSVReader(new FileReader(csvFilename));
            List content = csvReader.readAll();

            for (Object object : content) {
                row = (String[]) object;
                count++;
                ////Toast.makeText(getApplication(),"Syncing..3 of 4...",Toast.LENGTH_SHORT).show();
                //Toast.makeText(getApplicationContext(), "3 of 3", Toast.LENGTH_SHORT).show();
                //upload_mer_qnty_reg();
            }

            down_coun_cig=count-1;
//...
            csvReader.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void count_roun_neck() {
        try {
            String[] row = null;
            int count = 0;
            String csvFilename = "/sdcard/spin_it/ded_round_neck.csv";

            CSVReader csvReader = new CSVReader(new FileReader(csvFilename));
            List content = csvReader.readAll();

            for (Object object : content) {
                row = (String[]) object;
                count++;
                ////Toast.makeText(getApplication(),"Syncing..3 of 4...",Toast.LENGTH_SHORT).show();
                //Toast.makeText(getApplicationContext(), "3 of 3", Toast.LENGTH_SHORT).show();
                //upload_mer_qnty_reg();
            }

            down_coun_round=count-1;
//...
            csvReader.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void count_polo() {
        try {
            String[] row = null;
            int count = 0;
            String csvFilename = "/sdcard/spin_it/ded_polo.csv";

            CSVReader csvReader = new CSVReader(new FileReader(csvFilename));
            List content = csvReader.readAll();

            for (Object object : content) {
                row = (String[]) object;
                count++;
                ////Toast.makeText(getApplication(),"Syncing..3 of 4...",Toast.LENGTH_SHORT).show();
                //Toast.makeText(getApplicationContext(), "3 of 3", Toast.LENGTH_SHORT).show();
                //upload_mer_qnty_reg();
            }

            down_coun_pol=count-1;
//...
            csvReader.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void count_cap() {
        try {
            String[] row = null;
            int count = 0;
            String csvFilename = "/sdcard/spin_it/ded_caps.csv";

            CSVReader csvReader = new CSVReader(new FileReader(csvFilename));
            List content = csvReader.readAll();

            for (Object object : content) {
                row = (String[]) object;
                count++;
                ////Toast.makeText(getApplication(),"Syncing..3 of 4...",Toast.LENGTH_SHORT).show();
                //Toast.makeText(getApplicationContext(), "3 of 3", Toast.LENGTH_SHORT).show();
                //upload_mer_qnty_reg();
            }

            down_coun_cap=count-1;
//...
            csvReader.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void count_match_box() {
        try {
            String[] row = null;
            int count = 0;
            String csvFilename = "/sdcard/spin_it/ded_box.csv";

            CSVReader csvReader = new CSVReader(new FileReader(csvFilename));
            List content = csvReader.readAll();

            for (Object object : content) {
                row = (String[]) object;
                count++;
                ////Toast.makeText(getApplication(),"Syncing..3 of 4...",Toast.LENGTH_SHORT).show();
                //Toast.makeText(getApplicationContext(), "3 of 3", Toast.LENGTH_SHORT).show();
                //upload_mer_qnty_reg();
            }

            down_coun_match=count-1;
//...
            csvReader.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void calculate_qnty() {

        //cur_qty_polo=in_qnty_pol-af_qnty_pol;

        find_initial_qnt();
       // str_amount_owed_id = Double.parseDouble(xp_amount_owed);
        double in_cap= Double.parseDouble(in_qnty_cap);
        double in_ring= Double.parseDouble(in_qnty_ring);
        double in_neck= Double.parseDouble(in_qnty_round);
        double in_cig= Double.parseDouble(in_qnty_cig);
        double in_box= Double.parseDouble(in_qnty_match);
        double in_polo= Double.parseDouble(in_qnty_pol);

        double af_cap= Double.parseDouble(af_qnty_cap);
        double af_ring= Double.parseDouble(af_qnty_ring);
        double af_neck= Double.parseDouble(af_qnty_round);
        double af_cig= Double.parseDouble(af_qnty_cig);
        double af_box= Double.parseDouble(af_qnty_match);
        double af_polo= Double.parseDouble(af_qnty_pol);

        cur__qnt_cap=in_cap-af_cap;
        cur_qnt_key_ring=in_ring-af_ring;
        cur_qnt_round_nec=in_neck-af_neck;
        cur_qqnt_cigaretes=in_cig-af_cig;
        cur_qty_match_box=in_box-af_box;
        cur_qty_polo=in_polo-af_polo;

        save_current();
    }
*/
   /* private void save_current() {

        clear();
        TelephonyManager tm = (TelephonyManager)getSystemService(TELEPHONY_SERVICE);
       String imei2= tm.getDeviceId();

        formattedDate = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(Calendar.getInstance().getTime());
        //opens andor creates the file students.txt in the folder /storage/emulated/0/Android/data/<your package name>/files/
        File file = new File("/sdcard/spin_it/current_in_stock.csv");
        //one_b=one_bee.getText().toString();
        try {
            //create a filewriter and set append modus to true

            FileWriter fw = new FileWriter(file, true);
           // fw.append(System.getProperty("line.separator"));

            fw.append("IMEI:"+ imei2+",");
            fw.append("T SHIRTS:"+ cur_qty_match_box+",");
            fw.append("CAPS:"+ cur__qnt_cap+",");
            fw.append("SELFIE STICK:"+ cur_qqnt_cigaretes+",");
            fw.append("BACK PACK:"+ cur_qty_polo+",");
            fw.append("REFLECTOR JACKET:"+ cur_qnt_key_ring+",");
            fw.append("MOBILE STICKER:"+ cur_qnt_round_nec+",");
            fw.append("DATETIME:" + formattedDate + "");
            // fw.append(System.getProperty("line.separator"));
            // fw.append("QN1b:" + one_b + "--NEXT QNS--");

            fw.close();
           // end();

        } catch (IOException e) {
            Log.w("ExternalStorage", "Error writing " + file, e);
        }

    }

    private void clear() {
        try {
            FileWriter fw = new FileWriter("/sdcard/spin_it/current_in_stock.csv/", false);
            PrintWriter pw = new PrintWriter(fw, false);
            pw.flush();
            pw.close();
            fw.close();
        } catch (IOException e) {
            Log.w("ExternalStorage", "Error writing ", e);
        }

    }

    private void find_initial_qnt() {
        try {
            String[] row = null;
            String csvFilename = "/sdcard/spin_it/merc_quantity.csv";

            CSVReader csvReader = new CSVReader(new FileReader(csvFilename));
            List content = csvReader.readAll();

            for (Object object : content) {
                row = (String[]) object;
                in_qnty_match=row[0];
                in_qnty_cap=row[1];
                in_qnty_pol=row[2];
                in_qnty_round=row[3];
                in_qnty_cig=row[4];
                in_qnty_ring =row[5];

               // in_qnty_ring =row[4];
                //Toast.makeText(getApplication(),"Syncing..3 of 4...",Toast.LENGTH_SHORT).show();
             ///   Toast.makeText(getApplicationContext(), "fetched_init", Toast.LENGTH_SHORT).show();
               // upload_mer_qnty_reg();
            }
//...
            csvReader.close();
            find_aft_qnt();//////////////////////////////////hre///////////////////////////////////////////////////////////////////////////////
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    private void find_aft_qnt() {
        try {
            String[] row = null;
            String csvFilename = "/sdcard/spin_it/after_quantity.csv";

            CSVReader csvReader = new CSVReader(new FileReader(csvFilename));
            List content = csvReader.readAll();

            for (Object object : content) {
                row = (String[]) object;
                af_qnty_match=row[0];
                af_qnty_cap=row[1];
                af_qnty_pol=row[2];
                af_qnty_round=row[3];
                af_qnty_cig=row[4];
                af_qnty_ring =row[5];

                // in_qnty_ring =row[4];
                //Toast.makeText(getApplication(),"Syncing..3 of 4...",Toast.LENGTH_SHORT).show();
                //Toast.makeText(getApplicationContext(), "fetched_init", Toast.LENGTH_SHORT).show();
                // upload_mer_qnty_reg();
            }
//...
            csvReader.close();
        } catch (IOException e) {
            e.printStackTrace();
        }


    }*/
    private void start_spin() {

spin_sound();

        List<String> list = new ArrayList<String>();

        list.add("1245");
        list.add("1567");
        list.add("16985");
        list.add("17447");
        list.add("3834");
        list.add("4334");
        list.add("11465");
        list.add("380");
        list.add("20474");
        list.add("348");
        list.add("345");
        list.add("890");
        list.add("567");
        list.add("568");
        list.add("109");
        list.add("689");
        list.add("12564");
        list.add("10453");
        list.add("14245");
        list.add("15567");
        list.add("566");
        list.add("18281");
        list.add("19457");
        list.add("945");
        list.add("13654");
        list.add("456");
        list.add("3807");
        list.add("5463");
        list.add("6464");
        list.add("7564");
        list.add("874");
        list.add("8574");
        list.add("9645");



        Random randomizer = new Random();
        random = list.get(randomizer.nextInt(list.size()));

        //float f = Float.parseFloat(s);

        Rota=Float.parseFloat(random);
        mCurrRotation %= 360;
        float fromRotation = mCurrRotation;
        float toRotation = mCurrRotation += Rota;

        final RotateAnimation rotateAnim = new RotateAnimation(
                fromRotation, toRotation, spin_angle.getWidth()/2, spin_angle.getHeight()/2);

        rotateAnim.setDuration(5000); // Use 0 ms to rotate instantly
        rotateAnim.setFillAfter(true); // Must be true or the animation will reset

        spin_angle.startAnimation(rotateAnim);

        //starttimer();
        startTimer();

         //  }

       // }
    }

    private void spin_sound() {
        final MediaPlayer ourSong = MediaPlayer.create(getApplicationContext(), R.raw.spin);
        ourSong.start();
    }

/*    private void erase_status() {
        try {
            FileWriter fw = new FileWriter("/sdcard/spin_it/spin_staus.csv", false);
            PrintWriter pw = new PrintWriter(fw, false);
            pw.flush();
            pw.close();
            fw.close();
        } catch (IOException e) {
            Log.w("ExternalStorage", "Error writing ", e);
        }
    }

    private void inser_cannot() {
        File file = new File("/sdcard/spin_it/spin_staus.csv");
        //namepx=name_xc.getText().toString();
        //idpx=id_xc.getText().toString();
        //phonepx=phone_xc.getText().toString();
        try {
            //create a filewriter and set append modus to true

            FileWriter fw = new FileWriter(file, true);
            // fw.append(System.getProperty("line.separator"));

            fw.append(""+"not_ready"+",");
            fw.append(""+""+"spin");
            //  fw.append("USER ID NUMBER:"+ idpx+",");
            // fw.append("USER TEL:"+ phonepx+",");
            // fw.append("DATETIME:"+formattedDate+"");
            // fw.append(System.getProperty("line.separator"));
            // fw.append("QN1b:" + one_b + "--NEXT QNS--");

            fw.close();
          //  end();

        } catch (IOException e) {
            Log.w("ExternalStorage", "Error writing " + file, e);
        }
    }

    private void insert_default_values() {
        File file = new File("/sdcard/spin_it/spin_staus.csv");
        //namepx=name_xc.getText().toString();
        //idpx=id_xc.getText().toString();
        //phonepx=phone_xc.getText().toString();
        try {
            //create a filewriter and set append modus to true

            FileWriter fw = new FileWriter(file, true);
           // fw.append(System.getProperty("line.separator"));

            fw.append(""+"ready"+",");
            fw.append(""+"spin"+"");
          //  fw.append("USER ID NUMBER:"+ idpx+",");
           // fw.append("USER TEL:"+ phonepx+",");
           // fw.append("DATETIME:"+formattedDate+"");
            // fw.append(System.getProperty("line.separator"));
            // fw.append("QN1b:" + one_b + "--NEXT QNS--");

            fw.close();
            //start_spin();

        } catch (IOException e) {
            Log.w("ExternalStorage", "Error writing " + file, e);
        }
    }

    private void test_spin() {
        try {
            String[] row = null;
            String csvFilename = "/sdcard/spin_it/spin_staus.csv";

            CSVReader csvReader = new CSVReader(new FileReader(csvFilename));
            List content = csvReader.readAll();

            for (Object object : content) {
                row = (String[]) object;
                spin_status=row[0];

               // Toast.makeText(getApplicationContext(), "Syncing...4 of 4..", Toast.LENGTH_SHORT).show();


            }
//...
            csvReader.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }*/

    public void startTimer() {
        //set a new Timer
        timer = new Timer();

        //initialize the TimerTask's job
        initializeTimerTask();

        //schedule the timer, after the first 5000ms the TimerTask will run every 10000ms
        timer.schedule(timerTask, 5200, 4450000); //
        //startService(new Intent(UnusualService.this, UnusualService.class);
    }

    public void initializeTimerTask() {

        timerTask = new TimerTask() {
            public void run() {

                //use a handler to run a toast that shows the current timestamp
                handler.post(new Runnable() {
                    public void run() {
                        ///////

                        //new Loadtask().execute();
                        start_pop();
                    }
                });
            }
        };
    }

    private void start_pop() {
        ///-2
        if(Rota == 1567 || Rota == 4334||Rota == 380||Rota == 345 || Rota == 568||Rota == 3834){

            timer.cancel();
            timer.purge();
            AlertDialog.Builder builder = new AlertDialog.Builder(Spin_it.this);
            LayoutInflater inflater = Spin_it.this.getLayoutInflater();
            View dialogView = inflater.inflate(R.layout.lost_rt, null);
            builder.setView(dialogView)


                    .setNegativeButton(R.string.won, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                             merchendize_xc = "LOST";
                            end_4();
                             myInsertFunc();
                        }
                    }).create().show();

            ///-1
        }else if(Rota == 000 ){

            timer.cancel();
            timer.purge();
            AlertDialog.Builder builder = new AlertDialog.Builder(Spin_it.this);
            LayoutInflater inflater = Spin_it.this.getLayoutInflater();
            View dialogView = inflater.inflate(R.layout.lost_rt, null);
            builder.setView(dialogView)


                    .setNegativeButton(R.string.won, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                             merchendize_xc = "LOST";
                            end_4();
                            myInsertFunc();
                        }
                    }).create().show();


        }
        //1
        else if(Rota == 12564 ||Rota == 8574||Rota == 20474)
        {

            AlertDialog.Builder builder = new AlertDialog.Builder(Spin_it.this);
            LayoutInflater inflater = Spin_it.this.getLayoutInflater();
            View dialogView = inflater.inflate(R.layout.dialog, null);
            builder.setView(dialogView)


                    .setNegativeButton(R.string.won, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            merchendize_xc = "WINNER";

                            myInsertFunc();

                        }
                    }).create().show();



        }

        else if(Rota == 6464||Rota == 11465){
            //if(cur_qty_polo > 0){
            AlertDialog.Builder builder = new AlertDialog.Builder(Spin_it.this);
            LayoutInflater inflater = Spin_it.this.getLayoutInflater();
            View dialogView = inflater.inflate(R.layout.dialog2, null);
            builder.setView(dialogView)


                    .setNegativeButton(R.string.won, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            merchendize_xc = "WINNER";

                            myInsertFunc();
                           /// ded_polo();
                        }
                    }).create().show();


        }

        else if(Rota == 1245 || Rota == 16985||Rota == 17447){
            //if(cur_qnt_key_ring > 0){
            AlertDialog.Builder builder = new AlertDialog.Builder(Spin_it.this);
            LayoutInflater inflater = Spin_it.this.getLayoutInflater();
            View dialogView = inflater.inflate(R.layout.dialog3, null);
            builder.setView(dialogView)


                    .setNegativeButton(R.string.won, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            merchendize_xc = "WINNER";

                            myInsertFunc();
                           // ded_ring();
                        }
                    }).create().show();


        }

        else if(Rota == 348 || Rota == 890||Rota == 567||Rota == 109){
            // if(cur__qnt_cap > 0){
            AlertDialog.Builder builder = new AlertDialog.Builder(Spin_it.this);
            LayoutInflater inflater = Spin_it.this.getLayoutInflater();
            View dialogView = inflater.inflate(R.layout.dialog4, null);
            builder.setView(dialogView)


                    .setNegativeButton(R.string.won, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                           // merchendize_xc = "CAP";
                            merchendize_xc = "WINNER";
                            myInsertFunc();
                            ///ded_cap();
                        }
                    }).create().show();

        }

        else if(Rota == 14245 || Rota == 15567||Rota == 18281){

            AlertDialog.Builder builder = new AlertDialog.Builder(Spin_it.this);
            LayoutInflater inflater = Spin_it.this.getLayoutInflater();
            View dialogView = inflater.inflate(R.layout.dialog5, null);
            builder.setView(dialogView)


                    .setNegativeButton(R.string.won, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            merchendize_xc = "WINNER";

                            myInsertFunc();

                        }
                    }).create().show();

        }

        else if(Rota == 7564 || Rota == 874||Rota == 9645){
           // if(cur_qqnt_cigaretes> 0){
            AlertDialog.Builder builder = new AlertDialog.Builder(Spin_it.this);
            LayoutInflater inflater = Spin_it.this.getLayoutInflater();
            View dialogView = inflater.inflate(R.layout.dialog6, null);
            builder.setView(dialogView)

                    .setNegativeButton(R.string.won, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            merchendize_xc = "WINNER";

                            myInsertFunc();
                        }
                    }).create().show();

        }
        ///////////////////////////////////////////////
        else if(Rota == 689|| Rota == 19457||Rota == 566){
            // if(cur_qqnt_cigaretes> 0){
            AlertDialog.Builder builder = new AlertDialog.Builder(Spin_it.this);
            LayoutInflater inflater = Spin_it.this.getLayoutInflater();
            View dialogView = inflater.inflate(R.layout.dialog7, null);
            builder.setView(dialogView)

                    .setNegativeButton(R.string.won, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            merchendize_xc = "WINNER";

                            myInsertFunc();
                        }
                    }).create().show();

        }
        else if(Rota == 945 || Rota == 456||Rota == 13654){
            // if(cur_qqnt_cigaretes> 0){
            AlertDialog.Builder builder = new AlertDialog.Builder(Spin_it.this);
            LayoutInflater inflater = Spin_it.this.getLayoutInflater();
            View dialogView = inflater.inflate(R.layout.dialog8, null);
            builder.setView(dialogView)

                    .setNegativeButton(R.string.won, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            merchendize_xc = "WINNER";

                            myInsertFunc();
                        }
                    }).create().show();

        }
        else if(Rota == 10453||Rota == 3807){
            // if(cur_qqnt_cigaretes> 0){
            AlertDialog.Builder builder = new AlertDialog.Builder(Spin_it.this);
            LayoutInflater inflater = Spin_it.this.getLayoutInflater();
            View dialogView = inflater.inflate(R.layout.dialog9, null);
            builder.setView(dialogView)

                    .setNegativeButton(R.string.won, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            merchendize_xc = "WINNER";

                            myInsertFunc();
                        }
                    }).create().show();

        }
        else if(Rota == 5463){
            // if(cur_qqnt_cigaretes> 0){
            AlertDialog.Builder builder = new AlertDialog.Builder(Spin_it.this);
            LayoutInflater inflater = Spin_it.this.getLayoutInflater();
            View dialogView = inflater.inflate(R.layout.dialog10, null);
            builder.setView(dialogView)

                    .setNegativeButton(R.string.won, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            merchendize_xc = "WINNER";

                            myInsertFunc();
                        }
                    }).create().show();

        }

        else{
            timer.cancel();
            timer.purge();
            AlertDialog.Builder builder = new AlertDialog.Builder(Spin_it.this);
            LayoutInflater inflater = Spin_it.this.getLayoutInflater();
            View dialogView = inflater.inflate(R.layout.lost_rt, null);
            builder.setView(dialogView)


                    .setNegativeButton(R.string.won, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                             merchendize_xc = "LOST";
                            end_4();
                             myInsertFunc();
                        }
                    }).create().show();

        }
    }

   /* private void ded_match() {
        File file = new File("/sdcard/spin_it/ded_box.csv");
        //one_b=one_bee.getText().toString();
        try {
            //create a filewriter and set append modus to true

            FileWriter fw = new FileWriter(file, true);
            fw.append(System.getProperty("line.separator"));
            //fw.append("########### START SURVEY ########"+"");
            //fw.
            //fw.append(System.getProperty("line.separator"));
            fw.append("BOX,BOX");
          //  fw.append("ID NUMBER:"+ id_number+",");
           // fw.append("MERCHANDIZE:"+ merchendize_xc+",");
            //fw.append("DATETIME:"+ formattedDate+"");
            // fw.append(System.getProperty("line.separator"));
            // fw.append("QN1b:" + one_b + "--NEXT QNS--");

            fw.close();
            end();

        } catch (IOException e) {
            Log.w("ExternalStorage", "Error writing " + file, e);
        }
    }
*/
   /* private void ded_cap() {
        File file = new File("/sdcard/spin_it/ded_caps.csv");
        //one_b=one_bee.getText().toString();
        try {
            //create a filewriter and set append modus to true

            FileWriter fw = new FileWriter(file, true);
            fw.append(System.getProperty("line.separator"));
            //fw.append("########### START SURVEY ########"+"");
            //fw.
            //fw.append(System.getProperty("line.separator"));
            fw.append("CAP,CAP");
          //  fw.append("ID NUMBER:"+ id_number+",");
          //  fw.append("MERCHANDIZE:"+ merchendize_xc+",");
         //   fw.append("DATETIME:"+ formattedDate+"");
            // fw.append(System.getProperty("line.separator"));
            // fw.append("QN1b:" + one_b + "--NEXT QNS--");

            fw.close();
            end();

        } catch (IOException e) {
            Log.w("ExternalStorage", "Error writing " + file, e);
        }
    }*/

   /* private void ded_polo() {
        File file = new File("/sdcard/spin_it/ded_polo.csv");
        //one_b=one_bee.getText().toString();
        try {
            //create a filewriter and set append modus to true

            FileWriter fw = new FileWriter(file, true);
            fw.append(System.getProperty("line.separator"));

            fw.append("POLO,POLO");
           // fw.append("ID NUMBER:"+ id_number+",");
          //  fw.append("MERCHANDIZE:"+ merchendize_xc+",");
          //  fw.append("DATETIME:"+ formattedDate+"");
            // fw.append(System.getProperty("line.separator"));
            // fw.append("QN1b:" + one_b + "--NEXT QNS--");

            fw.close();
            end();

        } catch (IOException e) {
            Log.w("ExternalStorage", "Error writing " + file, e);
        }
    }*/

   /* private void ded_round() {
        File file = new File("/sdcard/spin_it/ded_round_neck.csv");
        //one_b=one_bee.getText().toString();
        try {
            //create a filewriter and set append modus to true

            FileWriter fw = new FileWriter(file, true);
            fw.append(System.getProperty("line.separator"));
            //fw.append("########### START SURVEY ########"+"");
            //fw.
            //fw.append(System.getProperty("line.separator"));
            fw.append("ROUND_NECK,ROUND_NECK");
          //  fw.append("ID NUMBER:"+ id_number+",");
          //  fw.append("MERCHANDIZE:"+ merchendize_xc+",");
            //fw.append("DATETIME:"+ formattedDate+"");
            // fw.append(System.getProperty("line.separator"));
            // fw.append("QN1b:" + one_b + "--NEXT QNS--");

            fw.close();
            end();

        } catch (IOException e) {
            Log.w("ExternalStorage", "Error writing " + file, e);
        }
    }

    private void ded_ring() {
        File file = new File("/sdcard/spin_it/ded_ring.csv");
        //one_b=one_bee.getText().toString();
        try {
            //create a filewriter and set append modus to true

            FileWriter fw = new FileWriter(file, true);
            fw.append(System.getProperty("line.separator"));
            //fw.append("########### START SURVEY ########"+"");
            //fw.
            //fw.append(System.getProperty("line.separator"));
            fw.append("RING,RING");
            //fw.append("ID NUMBER:"+ id_number+",");
            ///fw.append("MERCHANDIZE:"+ merchendize_xc+",");
            //fw.append("DATETIME:"+ formattedDate+"");
            // fw.append(System.getProperty("line.separator"));
            // fw.append("QN1b:" + one_b + "--NEXT QNS--");

            fw.close();
            end();

        } catch (IOException e) {
            Log.w("ExternalStorage", "Error writing " + file, e);
        }
    }
*/
    private void try_again() {
        Toast.makeText(getApplicationContext(), "Try again !.", Toast.LENGTH_SHORT).show();
    }

    private void myInsertFunc() {
        formattedDate = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(Calendar.getInstance().getTime());
        //opens andor creates the file students.txt in the folder /storage/emulated/0/Android/data/<your package name>/files/
        File file = new File("/sdcard/yatta_spin/yatta_spin_2018.csv");
        //one_b=one_bee.getText().toString();
        try {
            //create a filewriter and set append modus to true

            FileWriter fw = new FileWriter(file, true);
            fw.append(System.getProperty("line.separator"));

            fw.append("" +imei_xc+ ",");
            fw.append("" +id_number+ ",");
            fw.append("" +merchendize_xc+ ",");//shop
           // fw.append("" +phonepx+ ",");//barcode
            fw.append("" + formattedDate + "");

            fw.close();
            end();

        } catch (IOException e) {
            Log.w("ExternalStorage", "Error writing " + file, e);
        }
    }
    private void myInsertFunc2() {
        formattedDate = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(Calendar.getInstance().getTime());
        //opens andor creates the file students.txt in the folder /storage/emulated/0/Android/data/<your package name>/files/
        File file = new File("/sdcard/yatta_spin/yatta_spin_2018.csv");
        //one_b=one_bee.getText().toString();
        try {
            //create a filewriter and set append modus to true

            FileWriter fw = new FileWriter(file, true);
            fw.append(System.getProperty("line.separator"));
            //fw.append("########### START SURVEY ########"+"");
            //fw.
            //fw.append(System.getProperty("line.separator"));
            fw.append("" +imei_xc+ ",");
            fw.append("" +id_number+ ",");
            fw.append("" +merchendize_xc+ ",");//shop
            // fw.append("" +phonepx+ ",");//barcode
            fw.append("" + formattedDate + "");
            // fw.append(System.getProperty("line.separator"));
            // fw.append("QN1b:" + one_b + "--NEXT QNS--");

            fw.close();
            end();

        } catch (IOException e) {
            Log.w("ExternalStorage", "Error writing " + file, e);
        }
    }

    private boolean end_4() {
        Toast.makeText(getApplicationContext(), "Ops!!", Toast.LENGTH_SHORT).show();
        Intent i = new Intent(getApplicationContext(), idle_window.class);
       // erase_status();
       // insert_default_values();
        startActivity(i);
        finish();
        return true;
    }

    private boolean end() {
        //update_after_qnty();
        Toast.makeText(getApplicationContext(), "Awarded !.", Toast.LENGTH_SHORT).show();
       /// update_after_qnty();
        //erase_status();
       // insert_default_values();
        Intent i = new Intent(getApplicationContext(), Register_user.class);
        startActivity(i);
        finish();
        // xname.setText("");
        // xemail.setText("");
        // xtel.setText("");
        return true;
    }

    private void setdata() {


        nane_xvccb.setText(new StringBuilder()
                // Month is 0 based, just add 1
                .append(id_name));

    }

    private void explode_sound() {


        final MediaPlayer ourSong = MediaPlayer.create(getApplicationContext(), R.raw.xplode_kk);
        ourSong.start();
        start_spin();
    }


    private Runnable mRunner = new Runnable() {
        @Override
        public void run() {
            mHandler.removeCallbacks(mRunner);
            mFX.invalidate();
        }
    };

}
