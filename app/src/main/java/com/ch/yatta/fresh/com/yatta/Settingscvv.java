package com.ch.chewing.fresh.com.fresh_chewing_gum;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Intent;
import android.os.Bundle;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.opencsv.CSVReader;

import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;

/**
 * Created by peter on 10/13/2016.
 */

public class Settingscvv extends Activity {
    EditText merc_round_neck;
    EditText merc_cigarette;
    EditText merc_ring;
    EditText merc_match_box;
    EditText merc_cap_txt;
    EditText merc_polo;
    Button  btn_close_xc;
    Button btn_set_xc;
    Button btn_submit_xc;
    String imei2;
    String n_xc_namepx;
    String descpx;
    String qntpx;
    String formattedDate;

    TextView merc_match_boxx;
    TextView merc_cap_txtx;
    TextView merc_polox;
    TextView merc_round_neckx;
    TextView merc_cigarettex;
    TextView merc_ringx;

    String cur_qnt_round_nec;
    String cur_qty_match_box;
    String cur_qqnt_cigaretes;
    String cur__qnt_cap;
    String cur_qnt_key_ring;
    String cur_qty_polo;
    String cur_qnt_date;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.register_merc);


        merc_match_box=(EditText) findViewById(R.id.match_box);
        merc_cap_txt=(EditText) findViewById(R.id.cap_txt);
        merc_polo=(EditText) findViewById(R.id.polo_xc);
        merc_round_neck=(EditText) findViewById(R.id.round_neck);
        merc_cigarette=(EditText) findViewById(R.id.cigarette);
        merc_ring=(EditText) findViewById(R.id.ring_cp);

        merc_match_boxx=(TextView) findViewById(R.id.idx1);
         merc_cap_txtx=(TextView) findViewById(R.id.idx2);
        merc_polox=(TextView) findViewById(R.id.idx3);
        merc_round_neckx=(TextView) findViewById(R.id.idx4);
        merc_cigarettex=(TextView) findViewById(R.id.idx5);
        merc_ringx=(TextView) findViewById(R.id.idx6);

        btn_submit_xc=(Button) findViewById(R.id.btn_button_sub);
        btn_set_xc=(Button) findViewById(R.id.btn_back);
        btn_close_xc=(Button) findViewById(R.id.btn_closexc);

        //name_xc=(EditText) findViewById(R.id.txt_name);

        btn_set_xc.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(getApplicationContext(), Register_user.class);
                startActivity(i);
               // finish();

            }
        });
        btn_close_xc.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
             finish();
            }
        });

        btn_submit_xc.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                myInsertFunc();
            }
        });


        set_data();
    }

    private void set_data() {

        try {
            String[] row = null;
            String csvFilename = "/sdcard/spin_it/current_in_stock.csv";

            CSVReader csvReader = new CSVReader(new FileReader(csvFilename));
            List content = csvReader.readAll();

            for (Object object : content) {
                row = (String[]) object;
               // cur_imei=row[0];
                cur_qty_match_box=row[1];
                cur__qnt_cap=row[2];
                cur_qqnt_cigaretes=row[3];
                cur_qty_polo=row[4];
                cur_qnt_key_ring=row[5];
                cur_qnt_round_nec=row[6];
               // cur_qnt_date=row[7];


            }
//...
            csvReader.close();
            merc_match_boxx.setText(new StringBuilder()
                    // Month is 0 based, just add 1
                    .append(cur_qty_match_box));
            merc_cap_txtx.setText(new StringBuilder()
                    // Month is 0 based, just add 1
                    .append(cur__qnt_cap));
            merc_cigarettex.setText(new StringBuilder()
                    // Month is 0 based, just add 1
                    .append(cur_qqnt_cigaretes));
            merc_polox.setText(new StringBuilder()
                    // Month is 0 based, just add 1
                    .append(cur_qty_polo));
            merc_ringx.setText(new StringBuilder()
                    // Month is 0 based, just add 1
                    .append(cur_qnt_key_ring));
            merc_round_neckx.setText(new StringBuilder()
                    // Month is 0 based, just add 1
                    .append(cur_qnt_round_nec));


        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private boolean myInsertFunc() {
        erase();
        final AlertDialog.Builder ad = new AlertDialog.Builder(this);

        ad.setTitle("Error! ");
        ad.setIcon(android.R.drawable.btn_star_big_on);
        ad.setPositiveButton("Close", null);
        if(merc_match_box.getText().length() ==0)
        {
            ad.setMessage("Match box required!!");
            ad.show();
            merc_match_box.requestFocus();
            return false;
        }
        if( merc_cap_txt.getText().length() ==0)
        {
            ad.setMessage("Cap required!!");
            ad.show();
            merc_cap_txt.requestFocus();
            return false;
        }
        if(merc_polo.getText().length() ==0)
        {
            ad.setMessage("Polo tshirt required!!");
            ad.show();
            merc_polo.requestFocus();
            return false;
        }
        if(merc_round_neck.getText().length() ==0)
        {
            ad.setMessage("Round neck tshirt required!!");
            ad.show();
            merc_round_neck.requestFocus();
            return false;
        }
        if(merc_cigarette.getText().length() ==0)
        {
            ad.setMessage("Cigarette required!!");
            ad.show();
            merc_cigarette.requestFocus();
            return false;
        }
        if(merc_ring.getText().length() ==0)
        {
            ad.setMessage("Key ring required!!");
            ad.show();
            merc_ring.requestFocus();
            return false;
        }
        formattedDate = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(Calendar.getInstance().getTime());
        //opens andor creates the file students.txt in the folder /storage/emulated/0/Android/data/<your package name>/files/
        TelephonyManager tm = (TelephonyManager)getSystemService(TELEPHONY_SERVICE);
        imei2= tm.getDeviceId();

        File file = new File("/sdcard/spin_it/merc_quantity.csv");
         String match_box=merc_match_box.getText().toString();
        String mer_cap=merc_cap_txt.getText().toString();
         String pol_xc=merc_polo.getText().toString();
       String rou_neck=merc_round_neck.getText().toString();
        String cigarette=merc_cigarette.getText().toString();
        String ring_key=merc_ring.getText().toString();
        try {
            //create a filewriter and set append modus to true

            FileWriter fw = new FileWriter(file, true);
           // fw.append(System.getProperty("line.separator"));

            fw.append(match_box+",");
            fw.append(mer_cap+",");
            fw.append(pol_xc+",");
            fw.append(rou_neck+",");
            fw.append(cigarette+",");
            fw.append(ring_key+",");
            fw.append("date:"+ formattedDate+"");

           // fw.append("DATETIME:"+ phonepx+",");
            // fw.append(System.getProperty("line.separator"));
            // fw.append("QN1b:" + one_b + "--NEXT QNS--");

            fw.close();
            end();

        } catch (IOException e) {
            Log.w("ExternalStorage", "Error writing " + file, e);
        }
        return true;
    }

    private void erase() {
        try {
            FileWriter fw = new FileWriter("/sdcard/spin_it/merc_quantity.csv/", false);
            PrintWriter pw = new PrintWriter(fw, false);
            pw.flush();
            pw.close();
            fw.close();
        } catch (IOException e) {
            Log.w("ExternalStorage", "Error writing ", e);
        }
    }

    private void end() {
        Intent i=new Intent(getApplicationContext(), Register_user.class);
       // i.putExtra("sx_imei", imei2);
       // i.putExtra("sx_name", namepx);
      //  i.putExtra("sx_id", idpx);

        startActivity(i);
        finish();

    }
}
