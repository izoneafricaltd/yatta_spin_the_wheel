package com.ch.chewing.fresh.com.fresh_chewing_gum;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Canvas;
import android.graphics.Typeface;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.os.Handler;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.view.animation.Animation;
import android.view.animation.Animation.AnimationListener;
import android.view.animation.AnimationUtils;
import android.widget.TextView;
import android.widget.Toast;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.StatusLine;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

/**
 * Created by peter on 10/13/2016.
 */

public class Register_user extends Activity {
    EditText name_xc;
    EditText id_xc;
    EditText phone_xc;
    Button  btn_close_xc;
    Button  btn_test_xc;
    Button btn_set_xc,btn_submit_xc;
   // ImageView btn_submit_xc;
    String imei2;
    String namepx;
    Animation animBounce;
    String idpx;
    String phonepx;
    Typeface tf1;

    String formattedDate;
    String REG_DETAILS_IMEI;
    String REG_DETAILS_FULL_NAME;
    String REG_DETAILS_ID_NUMBER;
    String REG_DETAILS_TEL;
    String REG_DETAILS_DATETIMEX;
    String ACC_IMEIX;
    String ACC_USERNAMEXP;
    String ACC_PASSWORD;
    String ACC_DATETIME;
    String WON_IMEI_XP;
    String WON_ID_NO_XP;
    String WON_MER_NAME_XP;
    String WON_DATETIME_XP;
    String MER_QNTY_IMEI;
    String MER_QNTY_NAME;
    String MER_QNTY_DESC;
    String MER_QNTY_QNTY;
    String MER_QNTY_DATE;
    ConnectionDetector cd;
    //String csvFile = "/sdcard/spin_it/register_details.csv";
    BufferedReader br = null;
    String line = "";
    String cvsSplitBy = ",";
    Timer timer;
    TimerTask timerTask;
    private final static int NUM_PARTICLES = 25;
    private final static int FRAME_RATE = 30;
    private final static int LIFETIME = 300;
    private Handler mHandler;
    private Handler mHandler2;
    private View mFX;
    private View mFX2;
    private View v2;
   // Animation animZoomIn;
   TextView txtheader;
    private View mFX_main;
    private Explosion mExplosion;
    ImageView iv;
    Button spinitx;
    ImageView spinitx2;
    ImageView spinitx3;
    //ImageView spin_angle;
   // final Handler handler = new Handler();

    final Handler handler = new Handler();
    final Handler handler2 = new Handler();
    @SuppressLint("NewApi")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.register_names);

        RelativeLayout layout = (RelativeLayout) findViewById(R.id.frame);
        spinitx = (Button) findViewById(R.id.frsh_pack);
      //  spinitx2 = (ImageView) findViewById(R.id.wrtx);
       // spinitx3 = (ImageView) findViewById(R.id.wrtx3);

        mFX = new View(this) {
            @Override
            protected void onDraw(Canvas c) {
                if (mExplosion!=null && !mExplosion.isDead())  {
                    mExplosion.update(c);
                    mHandler.removeCallbacks(mRunner);
                    mHandler.postDelayed(mRunner, FRAME_RATE);
                } else if (mExplosion!=null && mExplosion.isDead()) {
                    spinitx.setAlpha(1f);
                }
                super.onDraw(c);
            }
        };
        mFX.setLayoutParams(new RelativeLayout.LayoutParams(
                RelativeLayout.LayoutParams.MATCH_PARENT,
                RelativeLayout.LayoutParams.MATCH_PARENT));
        layout.addView(mFX);
        mHandler = new Handler();


        /////////////////////////////////////////////////////////////////




        spinitx.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mExplosion == null || mExplosion.isDead()) {
                    int[] loc = new int[2];
                    v.getLocationOnScreen(loc);
                    int offsetX = (int) (loc[0] + (v.getWidth() * .25));
                    int offsetY = (int) (loc[1] - (v.getHeight() * .5));
                    mExplosion = new Explosion(NUM_PARTICLES, offsetX, offsetY, getApplicationContext());
                    mHandler.removeCallbacks(mRunner);
                    mHandler.post(mRunner);

                    mHandler.removeCallbacks(mRunner);
                    mHandler.post(mRunner);
                    v.animate().alpha(0).setDuration(LIFETIME).start();
                    explode_sound();
                }

            }
        });


        name_xc=(EditText) findViewById(R.id.txt_name);
        id_xc=(EditText) findViewById(R.id.txt_id_no);
        phone_xc=(EditText) findViewById(R.id.txt_id_phone);
        //btn_submit_xc=(Button) findViewById(R.id.frsh_pack);
        btn_set_xc=(Button) findViewById(R.id.btn_settings);
        btn_close_xc=(Button) findViewById(R.id.btn_closexc);
        btn_test_xc=(Button) findViewById(R.id.btn_test);
        txtheader= (TextView) findViewById(R.id.txt_fill_details);

        tf1=Typeface.createFromAsset(getAssets(),"bernierregular.ttf");
        name_xc.setTypeface(tf1);
        id_xc.setTypeface(tf1);
        txtheader.setTypeface(tf1);
        phone_xc.setTypeface(tf1);
        spinitx.setTypeface(tf1);
       btn_set_xc.setTypeface(tf1);
        btn_close_xc.setTypeface(tf1);
        btn_test_xc.setTypeface(tf1);


        //name_xc=(EditText) findViewById(R.id.txt_name);

       btn_set_xc.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                cd = new ConnectionDetector(getApplicationContext());

                // Check for internet connection
                if (!cd.isConnectingToInternet()) {

                    Intent i = new Intent(getApplicationContext(), All_confirmed_visitors.class);
                    startActivity(i);
                    finish();
                }else {

                    Intent i = new Intent(getApplicationContext(), All_confirmed_visitors.class);
                    startActivity(i);
                    finish();

                }

            }
        });
        btn_test_xc.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //stopService(new Intent(getBaseContext(), sync_service.class));
                startService(new Intent(getBaseContext(), sync_service.class));


            }
        });
        btn_close_xc.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
             finish();
            }
        });

       /* btn_submit_xc.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });*/
       // animBounce = AnimationUtils.loadAnimation(getApplicationContext(),
          //      R.anim.bounce);

        // set animation listener
       // animBounce.setAnimationListener(this);



       // animZoomIn = AnimationUtils.loadAnimation(getApplicationContext(),
              //  R.anim.zoom_in);

        // set animation listener
       // animZoomIn.setAnimationListener(this);
      //  startTimer();
        //startTimer3();
    }









    private void explode_sound() {
        final MediaPlayer ourSong = MediaPlayer.create(getApplicationContext(), R.raw.xplode_kk);
        ourSong.start();
        startTimer2();
    }

    private void startTimer2() {
        timer = new Timer();

        //initialize the TimerTask's job
        initializeTimerTask2();

        //schedule the timer, after the first 5000ms the TimerTask will run every 10000ms
        timer.schedule(timerTask, 1000, 61000000); //
    }

    private void initializeTimerTask2() {
        timerTask = new TimerTask() {
            public void run() {

                //use a handler to run a toast that shows the current timestamp
                handler.post(new Runnable() {
                    public void run() {
                        ///////

                        myInsertFunc();

                    }
                });
            }
        };
    }

    @Override
    protected void onPause() {
        super.onPause();

        //Intent i = new Intent(getApplicationContext(), idle_window.class);
       // startActivity(i);
        finish();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
       // Intent i = new Intent(getApplicationContext(), idle_window.class);
        //startActivity(i);
        finish();
    }

    public void startTimer() {
        //set a new Timer
        timer = new Timer();

        //initialize the TimerTask's job
        initializeTimerTask();

        //schedule the timer, after the first 5000ms the TimerTask will run every 10000ms
        timer.schedule(timerTask, 300000, 300000); //
        //startService(new Intent(UnusualService.this, UnusualService.class);
    }
    public void initializeTimerTask() {

        timerTask = new TimerTask() {
            public void run() {

                //use a handler to run a toast that shows the current timestamp
                handler.post(new Runnable() {
                    public void run() {
                        ///////

                        Intent i = new Intent(getApplicationContext(), idle_window.class);
                        startActivity(i);
                        finish();

                    }
                });
            }
        };
    }



    private boolean myInsertFunc() {

        final AlertDialog.Builder ad = new AlertDialog.Builder(this);

        ad.setTitle("Error! ");
        ad.setIcon(android.R.drawable.btn_star_big_on);
        ad.setPositiveButton("Close", null);

        if(name_xc.getText().length() ==0)
        {
            ad.setMessage("Name required!!");
            ad.show();
            name_xc.requestFocus();
            return false;
        }
        if(id_xc.getText().length() ==0)
        {
            ad.setMessage("Email required!!");
            ad.show();
            id_xc.requestFocus();
            return false;
        }
        if(phone_xc.getText().length() != 10)
        {
            ad.setMessage("Phone number should have ten characters!!");
            ad.show();
            phone_xc.requestFocus();
            return false;
        }
        formattedDate = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(Calendar.getInstance().getTime());
        //opens andor creates the file students.txt in the folder /storage/emulated/0/Android/data/<your package name>/files/
        TelephonyManager tm = (TelephonyManager)getSystemService(TELEPHONY_SERVICE);
        imei2= tm.getDeviceId();

        File file = new File("/sdcard/yatta_spin/yatta_spin_reg.csv");
         namepx=name_xc.getText().toString();
        idpx=id_xc.getText().toString();
         phonepx=phone_xc.getText().toString();
        try {
            FileWriter fw = new FileWriter(file, true);
            fw.append(System.getProperty("line.separator"));

            fw.append("" +imei2+ ",");
            fw.append("" +namepx+ ",");
            fw.append("" +idpx+ ",");//shop
            fw.append("" +phonepx+ ",");//barcode         
            fw.append("" + formattedDate + "");


            fw.close();
            Toast.makeText(getApplicationContext(), "Data Added Successfully!!", Toast.LENGTH_LONG).show();
            clear_text();
            end();

        } catch (IOException e) {
            Log.w("ExternalStorage", "Error writing " + file, e);
        }
        return true;
    }

    private void clear_text() {
        name_xc.setText("");
        id_xc.setText("");
        phone_xc.setText("");

    }

    private void end() {
        Intent i=new Intent(getApplicationContext(), Spin_it.class);
        i.putExtra("sx_imei", imei2);
        i.putExtra("sx_name", namepx);
        i.putExtra("sx_id", phonepx);

        startActivity(i);
      ///  erase_status();
       // insert_default_values();
        finish();

    }

  //  private void set_default() {
 /* private void erase_status() {
      try {
          FileWriter fw = new FileWriter("/sdcard/spin_it/spin_staus.csv", false);
          PrintWriter pw = new PrintWriter(fw, false);
          pw.flush();
          pw.close();
          fw.close();
      } catch (IOException e) {
          Log.w("ExternalStorage", "Error writing ", e);
      }
  }*/
       /* private void insert_default_values() {
            File file = new File("/sdcard/spin_it/spin_staus.csv");
            //namepx=name_xc.getText().toString();
            //idpx=id_xc.getText().toString();
            //phonepx=phone_xc.getText().toString();
            try {
                //create a filewriter and set append modus to true

                FileWriter fw = new FileWriter(file, true);
                // fw.append(System.getProperty("line.separator"));

                fw.append(""+"ready"+",");
                fw.append(""+"spin"+"");
                //  fw.append("USER ID NUMBER:"+ idpx+",");
                // fw.append("USER TEL:"+ phonepx+",");
                // fw.append("DATETIME:"+formattedDate+"");
                // fw.append(System.getProperty("line.separator"));
                // fw.append("QN1b:" + one_b + "--NEXT QNS--");

                fw.close();
                //start_spin();

            } catch (IOException e) {
                Log.w("ExternalStorage", "Error writing " + file, e);
            }
        }*/

   // }

   /* public String getHttpPost(String url,List<NameValuePair> params) {
        StringBuilder str = new StringBuilder();
        HttpClient client = new DefaultHttpClient();
        HttpPost httpPost = new HttpPost(url);

        try {
            httpPost.setEntity(new UrlEncodedFormEntity(params));
            HttpResponse response = client.execute(httpPost);
            StatusLine statusLine = response.getStatusLine();
            int statusCode = statusLine.getStatusCode();
            if (statusCode == 200) { // Status OK
                HttpEntity entity = response.getEntity();
                InputStream content = entity.getContent();
                BufferedReader reader = new BufferedReader(new InputStreamReader(content));
                String line;
                while ((line = reader.readLine()) != null) {
                    str.append(line);
                }
            } else {
                Log.e("Log", "Failed to download result..");
            }
        } catch (ClientProtocolException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return str.toString();
    }*/
    private Runnable mRunner = new Runnable() {
        @Override
        public void run() {
            mHandler.removeCallbacks(mRunner);
            mFX.invalidate();
        }
    };


}
