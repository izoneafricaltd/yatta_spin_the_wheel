package com.ch.chewing.fresh.com.fresh_chewing_gum;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.os.StrictMode;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.StatusLine;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;




public class f_Register_act extends Activity {

    ConnectionDetector cd;

    @SuppressLint("NewApi")
    @Override
    public void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.f_register);
        cd = new ConnectionDetector(getApplicationContext());

        // Check for internet connection
        if (!cd.isConnectingToInternet()) {
            // Internet Connection is not present
            //      alert.showAlertDialog(splash_tasks.this, "No Internet Connection",
            //      "Please connect to the internet.", false);
            //  alert.d
            // stop executing code by return
            // finish();
            // return;
            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setMessage("No internet connection!")
                    .setCancelable(false)
                    .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            //do things
                            finish();
                        }
                    });
            AlertDialog alert = builder.create();
            alert.show();
        }

        // Permission StrictMode
        if (android.os.Build.VERSION.SDK_INT > 9) {
            StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
            StrictMode.setThreadPolicy(policy);
        }

        // btnSave
        final Button btnSave = (Button) findViewById(R.id.btnRegister);
        // Perform action on click
        btnSave.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                if(SaveData())
                {
                    // When Save Complete
                }
            }
        });

        Button btn_link=(Button) findViewById(R.id.btnLink2);
        btn_link.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Intent i = new Intent(getApplicationContext(), f_Login_act.class);
                ///startActivity(i);

            }
        });

    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if(keyCode== KeyEvent.KEYCODE_BACK) {
            // Toast.makeText(getApplicationContext(), "back press", Toast.LENGTH_LONG).show();
        }else{

        }
        return false;
        // Disable back button..............
    }




    public boolean SaveData()
    {

        // txtUsername,txtPassword,txtName,txtEmail,txtTel
        final EditText txtfname = (EditText)findViewById(R.id.registerFName);
        final EditText txtlname = (EditText)findViewById(R.id.registerLName);
        final EditText txtEmail= (EditText)findViewById(R.id.registerEmail);
        final EditText txtConPassword = (EditText)findViewById(R.id.ConfPassword);
        final EditText txtPassword = (EditText)findViewById(R.id.registerPassword);
       // final EditText txtEmail = (EditText)findViewById(R.id.txtEmail);
        ///final EditText txtTel = (EditText)findViewById(R.id.txtTel);


        // Dialog
        final AlertDialog.Builder ad = new AlertDialog.Builder(this);

        ad.setTitle("Error! ");
        ad.setIcon(android.R.drawable.btn_star_big_on);
        ad.setPositiveButton("Close", null);

        // Check Username
        if(txtfname.getText().length() == 0)
        {
            ad.setMessage("Please input [First Name] ");
            ad.show();
            txtfname.requestFocus();
            return false;
        }
        if(txtlname.getText().length() == 0)
        {
            ad.setMessage("Please input [Last Name] ");
            ad.show();
            txtlname.requestFocus();
            return false;
        }
        // Check Password
        if(txtPassword.getText().length() == 0 || txtConPassword.getText().length() == 0 )
        {
            ad.setMessage("Please input [Password/Confirm Password] ");
            ad.show();
            txtPassword.requestFocus();
            return false;
        }
        if(txtPassword.getText().length() < 7)
        {
            ad.setMessage("Your password should be atleast 7 characters long");
            ad.show();
            txtPassword.requestFocus();
            return false;
        }

        // Check Password and Confirm Password (Match)
        if(!txtPassword.getText().toString().equals(txtConPassword.getText().toString()))
        {
            ad.setMessage("Password and Confirm Password Not Match! ");
            ad.show();
            txtConPassword.requestFocus();
            return false;
        }

        // Check Name
        if(txtEmail.getText().length() == 0)
        {
            ad.setMessage("Please input [Email] ");
            ad.show();
            txtEmail.requestFocus();
            return false;
        }
        // Check Email



        TelephonyManager tm = (TelephonyManager)getSystemService(TELEPHONY_SERVICE);
        String imei2= tm.getDeviceId();

        String url = "http://bluetrack.co.ke/task_app/register_account.php/";

        List<NameValuePair> params = new ArrayList<NameValuePair>();
        String p=txtfname.getText().toString();
        String y=p;

        params.add(new BasicNameValuePair("sImei", imei2));
        params.add(new BasicNameValuePair("sfname", txtfname.getText().toString()));
        params.add(new BasicNameValuePair("slname", txtlname.getText().toString()));
        params.add(new BasicNameValuePair("semail", txtEmail.getText().toString()));
        params.add(new BasicNameValuePair("spassword", txtPassword.getText().toString()));
        params.add(new BasicNameValuePair("slog", "logged_out"));

      //  params.add(new BasicNameValuePair("sEmail", txtEmail.getText().toString()));
      //  params.add(new BasicNameValuePair("sTel", txtTel.getText().toString()));


        /** Get result from Server (Return the JSON Code)
         * StatusID = ? [0=Failed,1=Complete]
         * Error	= ?	[On case error return custom error message]
         *
         * Eg Save Failed = {"StatusID":"0","Error":"Email Exists!"}
         * Eg Save Complete = {"StatusID":"1","Error":""}
         */

        String resultServer  = getHttpPost(url,params);

        /*** Default Value ***/
        String strStatusID = "0";
        String strError = "Internet problem!";

        JSONObject c;
        try {
            c = new JSONObject(resultServer);
            strStatusID = c.getString("StatusID");
            strError = c.getString("Error");
        } catch (JSONException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        // Prepare Save Data
        if(strStatusID.equals("0"))
        {
           // ad.setMessage(strError);
            //ad.show();

            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setMessage(strError)
                    .setCancelable(false)
                    .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            //do things

                           // Intent i = new Intent(getApplicationContext(), f_Login_act.class);
                           /// startActivity(i);
                            finish();
                           // finish();
                        }
                    });
            AlertDialog alert = builder.create();
            alert.show();

        }
        else
        {
            Toast.makeText(getApplicationContext(), "Registered Successfully", Toast.LENGTH_LONG).show();
          //  txtUsername.setText("");
            txtPassword.setText("");
            txtConPassword.setText("");
            txtfname.setText("");
            txtlname.setText("");
            txtEmail.setText("");
           // txtTel.setText("");

            //retun();
            //Intent i = new Intent(getApplicationContext(), f_Login_act.class);
           // startActivity(i);
            finish();
        }


        return true;
    }




    public String getHttpPost(String url,List<NameValuePair> params) {
        StringBuilder str = new StringBuilder();
        HttpClient client = new DefaultHttpClient();
        HttpPost httpPost = new HttpPost(url);

        try {
            httpPost.setEntity(new UrlEncodedFormEntity(params));
            HttpResponse response = client.execute(httpPost);
            StatusLine statusLine = response.getStatusLine();
            int statusCode = statusLine.getStatusCode();
            if (statusCode == 200) { // Status OK
                HttpEntity entity = response.getEntity();
                InputStream content = entity.getContent();
                BufferedReader reader = new BufferedReader(new InputStreamReader(content));
                String line;
                while ((line = reader.readLine()) != null) {
                    str.append(line);
                }
            } else {
                Log.e("Log", "Failed to download result..");
            }
        } catch (ClientProtocolException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return str.toString();
    }


    /*@Override
    public boolean onCreateOptionsMenu(Menu menu)
    {
        MenuInflater menuInflater = getMenuInflater();
        menuInflater.inflate(R.layout.menu_ya_lost, menu);
        return true;
    }*/





}
